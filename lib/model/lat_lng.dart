/*
Описание: Модель локации устройства
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class AppLatLng {
  final double lat;
  final double long;

  AppLatLng(this.lat, this.long);
}
