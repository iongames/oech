/*
Описание: Модель стартовой/конечной локации доставки
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class SendInfo {
  String address;
  String state;
  String phone;
  String other;

  SendInfo(this.address, this.state, this.phone, this.other);
}
