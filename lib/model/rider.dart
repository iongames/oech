/*
Описание: Модель доставщика
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class Rider {
  final String fullName;
  final String regNumber;
  final double rating;
  final String carModel;
  final String gender;
  final String phoneNumber;

  Rider(this.fullName, this.regNumber, this.rating, this.carModel, this.gender, this.phoneNumber);
}
