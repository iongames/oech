/*
Описание: Модель отзыва
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class Review {
  final String fullName;
  final String review;
  final double rating;

  Review(this.fullName, this.review, this.rating);
}
