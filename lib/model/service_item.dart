/*
Описание: Модель элемента сервсиов главного экрана
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class ServiceItem {
  final String title;
  final String content;
  final String asset;

  ServiceItem(this.title, this.content, this.asset);
}
