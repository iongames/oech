/*
Описание: Модель информации о доставке
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
import 'package:oech/model/send_info.dart';

class Delivery {
  final SendInfo startLocation;
  final List<SendInfo> endLocations;
  final String packageItems;
  final String packageWeight;
  final String packageWorth;
  final String trackingNumber;

  Delivery(
    this.startLocation,
    this.endLocations,
    this.packageItems,
    this.packageWeight,
    this.packageWorth,
    this.trackingNumber,
  );
}
