/*
Описание: Модель приветственного экрана
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class OnboardItem {
  final String title;
  final String content;
  final String asset;

  OnboardItem(this.title, this.content, this.asset);
}
