/*
Описание: Модель транзакции кошелька
Дата создания: 03.07.2023
Автор: Хасанов Георгий
*/
class Transaction {
  final String title;
  final String date;
  final String sum;
  final bool type;

  Transaction(this.title, this.date, this.sum, this.type);
}
