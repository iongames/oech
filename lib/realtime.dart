import 'dart:async';

import 'package:flutter/material.dart';

import 'main.dart';

class Realtime extends StatefulWidget {
  const Realtime({super.key});

  @override
  State<Realtime> createState() => _RealtimeState();
}

class _RealtimeState extends State<Realtime> {
  late final Stream<List<String>> _messagesStream;

  @override
  void initState() {
    final userUID = supabase.auth.currentUser!.id; // UID пользователя
    _messagesStream = supabase.from('messages').stream(primaryKey: ['id']).order('created_at').map(
          (maps) => maps.map((map) => Message.fromMap(map: map, myUserId: myUserId)).toList(), // Вместо Message свой fromJson
        );
    super.initState();
  }

//Отправка просто через insert:
  void sendMessage() async {
    await supabase.from('messages').insert(
      {
        'profile_id': "myUserId",
        'content': "text",
      },
    );
  }

//Для отображения реал тайма надо просто использовать Stream Builder:
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<String>>(
        stream: _messagesStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return SizedBox();
          } else {
            return SizedBox();
          }
        });
  }
}
