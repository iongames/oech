import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oech/view/colors.dart';
import 'package:oech/view/screens/login_screen.dart';
import 'package:oech/view/screens/main_screen.dart';
import 'package:oech/view/screens/onboard_screen.dart';
import 'package:oech/viewmodel/home_viewmodel.dart';
import 'package:oech/viewmodel/login_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import 'package:provider/provider.dart' as p;

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

/*
Описание: Точка входа в приложение
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
Future<void> main() async {
  await Supabase.initialize(
    url: 'https://qqdruecqbscnicnedwnv.supabase.co',
    anonKey:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InFxZHJ1ZWNxYnNjbmljbmVkd252Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODc5NDExNzAsImV4cCI6MjAwMzUxNzE3MH0.Q-dQdV0c20KlRh5waol13mSfklW0DKnfqU37YLTDMX8',
    authFlowType: AuthFlowType.pkce,
  );

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (create) => LoginViewModel()),
      ChangeNotifierProvider(create: (create) => HomeViewModel()),
    ],
    child: const MyApp(),
  ));
}

final supabase = Supabase.instance.client;

/*
Описание: Класс точки входа в приложение
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<StatefulWidget> createState() => MyAppState();
}

/*
Описание: State Класса точки входа в приложение
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class MyAppState extends State<MyApp> {
  late LoginViewModel viewModel;

  @override
  void initState() {
    super.initState();

    viewModel = p.Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'OECH',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: "Roboto",
        scaffoldBackgroundColor: Colors.white,
        hintColor: AppColors.lTextColor,
        cardColor: AppColors.lCardColor,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        scaffoldBackgroundColor: AppColors.dBackgroundColor,
        hintColor: Colors.white,
        cardColor: AppColors.dCardColor,
      ),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      home: FutureBuilder(
        future: viewModel.getSharedPrefs(),
        builder: (builder, snapshot) {
          if (snapshot.data != false) {
            return const OnboardScreen();
          } else {
            if (supabase.auth.currentSession != null) {
              return MainScreen();
            } else {
              return LoginScreen();
            }
          }
        },
      ),
    );
  }
}
