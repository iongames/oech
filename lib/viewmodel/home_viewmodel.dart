import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:oech/main.dart';
import 'package:oech/model/delivery.dart';
import 'package:oech/model/lat_lng.dart';
import 'package:oech/model/message.dart';
import 'package:oech/model/rider.dart';
import 'package:oech/model/send_info.dart';

import '../model/transaction.dart';
import '../view/widgets/app_alert_dialog.dart';

import 'package:http/http.dart' as http;

/*
Описание: Класс логики главных экранов приложения
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class HomeViewModel extends ChangeNotifier {
  AppLatLng loc = AppLatLng(0, 0);
  String name = "";

  List<Transaction> transactions = [];
  List<Rider> riderList = [];

  List<Message> messageList = [
    Message("Hello, Please check your phone, I just booked you to deliver my stuff", true),
    Message("Thank you for contacting me.", false),
    Message("I am already on my way to the pick up venue.", false),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
  ];

  Delivery? delivery;

  /*
  Описание: Загрузка данных доставки на сервер
  Дата создания: 02.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> createDelivery(Delivery delivery) async {
    try {
      // final data = await supabase.from("deliveries").insert({
      //   "startLocation": delivery.startLocation,
      //   "endLocations": delivery.endLocations, // ?
      //   "packageItems": delivery.packageItems,
      //   "packageWeight": delivery.packageWeight,
      //   "packageWorth": delivery.packageWorth,
      //   "trackingNumber": delivery.trackingNumber,
      // });

      Get.back();
      return true;
    } catch (e) {
      Get.back();
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));
      return false;
    }
  }

  /*
  Описание: Метод получения местоположения устройства
  Дата создания: 02.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> getCurrentLocation() async {
    try {
      LocationPermission permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied) {
        LocationPermission p = await Geolocator.requestPermission();

        if (p == LocationPermission.denied) {
          Get.dialog(AppAlertDialog(title: "ERROR", content: "Нет разрешения!"));

          return false;
        }
      }

      Position pos = await Geolocator.getCurrentPosition();

      loc = AppLatLng(pos.latitude, pos.longitude);

      final response = await http.get(Uri.parse("https://nominatim.openstreetmap.org/search?q=${loc.lat},${loc.long}&format=json"));

      if (response.statusCode == 200) {
        name = jsonDecode(response.body)[0]["display_name"];

        return true;
      } else {
        return false;
      }
    } catch (e) {
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));
      return false;
    }
  }

  /*
  Описание: Получение транзакций с сервера
  Дата создания: 02.07.2023
  Автор: Хасанов Георгий
  */
  Future<List<Transaction>> getTransactions() async {
    try {
      transactions.clear();
      final data = await supabase.from("transactions").select<List<dynamic>>();

      DateFormat format = DateFormat("MMM dd, yyyy");

      for (var t in data) {
        transactions.add(
          Transaction(
            t["title"],
            format.format(DateTime.parse(t["created_at"].toString())),
            NumberFormat("N###,###,###.00;-N###,###,###.00").format((t["price"] as double)),
            (t["price"] as double) >= 0,
          ),
        );
      }

      return transactions;
    } catch (e) {
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));

      return List<Transaction>.empty();
    }
  }

  /*
  Описание: Получение последней доставки с сервера
  Дата создания: 03.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> loadLastDelivery() async {
    try {
      //final data = await supabase.from("transactions").select<Map<String, dynamic>>().eq("id", userUID).single();

      delivery = Delivery(
        SendInfo("address", "state", "phone", "other"),
        List.from({SendInfo("address", "state", "phone", "other")}),
        "packageItems",
        "packageWeight",
        "packageWorth",
        "R-1233-1231-1234",
      );

      return true;
    } catch (e) {
      Get.back();
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));

      return false;
    }
  }

  /*
  Описание: Получение доставщиков с сервера
  Дата создания: 02.07.2023
  Автор: Хасанов Георгий
  */
  Future<List<Rider>> getRiders() async {
    try {
      riderList.clear();
      final data = await supabase.from("riders").select<List<dynamic>>();

      for (var r in data) {
        riderList.add(
          Rider(
            r["full_name"].toString(),
            r["reg_number"].toString(),
            r["rating"] as double,
            r["car_model"].toString(),
            r["gender"].toString(),
            r["phone"].toString(),
          ),
        );
      }

      return riderList;
    } catch (e) {
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));

      return List<Rider>.empty();
    }
  }
}
