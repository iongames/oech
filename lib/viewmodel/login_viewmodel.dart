import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oech/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../view/widgets/app_alert_dialog.dart';

/*
Описание: Класс логики экранов входа в приложение
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class LoginViewModel extends ChangeNotifier {
  /*
  Описание: Метод проверки повторного входа в приложение
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool?> getSharedPrefs() async {
    SharedPreferences shared = await SharedPreferences.getInstance();

    return shared.getBool("isFirstEnter");
  }

  /*
  Описание: Метод соъхранения повторного входа в приложение
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  Future<void> saveEnter() async {
    SharedPreferences shared = await SharedPreferences.getInstance();

    await shared.setBool("isFirstEnter", false);
  }

  /*
  Описание: Метод перехода на следующий экран
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  void jumpToNextPage(int index, PageController pageController) {
    pageController.jumpToPage(index + 1);
  }

  /*
  Описание: Метод регистрации в приложении
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> signUp(String email, String password, String phone, String name) async {
    try {
      final data = await supabase.auth.signUp(email: email, password: password, data: {"Phone": phone, "Full Name": name});

      return true;
    } catch (e) {
      Get.back();
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));

      return false;
    }
  }

  /*
  Описание: Метод входа в аккаунт в приложении
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> signIn(String email, String password) async {
    try {
      final data = await supabase.auth.signInWithPassword(email: email, password: password);

      Get.back();
      if (data.session != null) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      Get.back();
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));

      return false;
    }
  }

  /*
  Описание: Метод восстановления аккаунта в приложении
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> forgotPassword(String email) async {
    try {
      await supabase.auth.resetPasswordForEmail(email);

      Get.back();
      return true;
    } catch (e) {
      Get.back();
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));

      return false;
    }
  }

  /*
  Описание: Метод восстановления аккаунта в приложении
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> checkCode(String email, String code) async {
    try {
      await supabase.auth.verifyOTP(email: email, token: code, type: OtpType.recovery);

      Get.back();
      return true;
    } catch (e) {
      Get.back();
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));

      return false;
    }
  }

  /*
  Описание: Метод установки нового пароля
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> setNewPassword(String password) async {
    try {
      await supabase.auth.updateUser(UserAttributes(password: password));

      Get.back();
      return true;
    } catch (e) {
      Get.back();
      Get.dialog(AppAlertDialog(title: "ERROR", content: e.toString()));

      return false;
    }
  }
}
