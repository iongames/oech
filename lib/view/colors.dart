/*
Описание: Класс цветовой схемы приложения 
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
import 'dart:ui';

class AppColors {
  static const primaryColor = Color(0xFF0560FA);
  static const primaryVariantColor = Color(0xFF98C7FF);
  static const lPrimarySelectColor = Color(0xFFDDECFF);
  static const dPrimarySelectColor = Color(0xFF004393);
  static const secondaryColor = Color(0xFFEC8000);

  static const lTextColor = Color(0xFF3A3A3A);
  static const lCardColor = Color(0xFFCFCFCF);
  static const grayColor = Color(0xFFA7A7A7);

  static const lGrayColor = Color(0xFFF2F2F2);

  static const dCardColor = Color(0xFF001B3B);
  static const dBackgroundColor = Color(0xFF000D1D);

  static const warningColor = Color(0xFFEBBC2E);
  static const errorColor = Color(0xFFED3A3A);
  static const greenColor = Color(0xFF35B369);

  static const successDeliveryColor = Color(0xFF2F80ED);
}
