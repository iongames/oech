import 'package:flutter/material.dart';
import 'package:oech/model/onboard_item.dart';
import 'package:oech/view/screens/register_screen.dart';
import 'package:oech/viewmodel/login_viewmodel.dart';
import 'package:provider/provider.dart';

import '../widgets/onboard_component.dart';
import 'login_screen.dart';

/*
Описание: Класс приветственного экрана
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class OnboardScreen extends StatefulWidget {
  const OnboardScreen({super.key});

  @override
  State<OnboardScreen> createState() => _OnboardScreenState();
}

/*
Описание: State Класса приветственного экрана
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _OnboardScreenState extends State<OnboardScreen> {
  late LoginViewModel loginViewModel;

  final PageController _pageController = PageController();

  final List<OnboardItem> onboardList = [
    OnboardItem("Quick Delivery At Your Doorstep", "Enjoy quick pick-up and delivery to your destination", "assets/images/on_1.png"),
    OnboardItem("Flexible Payment", "Different modes of payment either before and after delivery without stress", "assets/images/on_2.png"),
    OnboardItem("Real-time Tracking", "Track your packages/items from the comfort of your home till final destination", "assets/images/on_3.png"),
  ];

  @override
  void initState() {
    super.initState();

    loginViewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: PageView.builder(
          itemCount: onboardList.length,
          controller: _pageController,
          itemBuilder: (itemBuilder, index) {
            return OnboardComponent(
              onboardItem: onboardList[index],
              itemIndex: index,
              onSkip: () async {
                await loginViewModel.saveEnter();
                Navigator.push(context, MaterialPageRoute(builder: (_) => const RegisterScreen()));
              },
              onNext: () {
                loginViewModel.jumpToNextPage(index, _pageController);
              },
              onLogin: () async {
                await loginViewModel.saveEnter();
                Navigator.push(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
              },
            );
          }),
    ));
  }
}
