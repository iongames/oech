import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:oech/model/delivery.dart';
import 'package:oech/view/screens/success_delivery_screen.dart';
import 'package:oech/view/screens/success_screen.dart';
import 'package:oech/view/widgets/app_button.dart';

import '../colors.dart';

/*
Описание: Класс экрана проверки информации завершенной доставки
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class PackagedDeliveriedScreen extends StatefulWidget {
  final Delivery delivery;

  const PackagedDeliveriedScreen({super.key, required this.delivery});

  @override
  State<PackagedDeliveriedScreen> createState() => _PackagedDeliveriedScreenState();
}

/*
Описание: State Класса экрана проверки информации завершенной доставки
Дата создания: 03.07.2023
Автор: Хасанов Георгий
*/
class _PackagedDeliveriedScreenState extends State<PackagedDeliveriedScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset("assets/icons/ic_back.svg"),
          ),
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
          automaticallyImplyLeading: false,
          toolbarHeight: 60,
          title: Text(
            "Package Delivered",
            style: TextStyle(fontSize: 16, color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : Theme.of(context).scaffoldBackgroundColor,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 24),
                Text(
                  "Package Information",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
                ),
                SizedBox(height: 8),
                Text(
                  "Origin details",
                  style:
                      TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.lTextColor : AppColors.primaryVariantColor),
                ),
                SizedBox(height: 4),
                Text(
                  "${widget.delivery.startLocation.address} ${widget.delivery.startLocation.state}",
                  style: TextStyle(
                    fontSize: 12,
                    color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  widget.delivery.startLocation.phone,
                  style: TextStyle(
                    fontSize: 12,
                    color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  "Destination details",
                  style:
                      TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.lTextColor : AppColors.primaryVariantColor),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(widget.delivery.endLocations.length, (index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${widget.delivery.endLocations[index].address} ${widget.delivery.endLocations[index].state}",
                          style: TextStyle(
                            fontSize: 12,
                            color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                          ),
                        ),
                        SizedBox(height: 4),
                        Text(
                          widget.delivery.endLocations[index].phone,
                          style: TextStyle(
                            fontSize: 12,
                            color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                          ),
                        ),
                        SizedBox(height: 8),
                      ],
                    );
                  }),
                ),
                Text(
                  "Other details",
                  style:
                      TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.lTextColor : AppColors.primaryVariantColor),
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Package Items",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      widget.delivery.packageItems,
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Weight of items",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      widget.delivery.packageWeight,
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tracking Number",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      widget.delivery.trackingNumber,
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 37),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 1,
                      color: AppColors.grayColor,
                    ))
                  ],
                ),
                SizedBox(height: 8),
                Text(
                  "Charges",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Delivery Charges",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      "N${NumberFormat("###,###,###.00").format(2500.00)}",
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Instant delivery",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      "N${NumberFormat("###,###,###.00").format(300.00)}",
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tax and Service Charges",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      "N${NumberFormat("###,###,###.00").format(200.00)}",
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 9),
                Row(
                  children: [
                    Expanded(
                      child: Card(
                        elevation: 2,
                        margin: EdgeInsets.zero,
                        shadowColor: Colors.black.withOpacity(0.5),
                        color: Colors.transparent,
                        shape: RoundedRectangleBorder(),
                        child: Container(
                          height: 1,
                          color: AppColors.grayColor,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Package total",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      "N${NumberFormat("###,###,###.00").format(3000.00)}",
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 24),
                Text(
                  "Click on  delivered for successful delivery and rate rider or report missing item",
                  style: TextStyle(fontSize: 12, color: AppColors.successDeliveryColor),
                ),
                SizedBox(height: 41),
                Row(
                  children: [
                    Expanded(
                      child: AppButton(
                        text: const Text("Report", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: AppColors.primaryColor)),
                        background: Theme.of(context).scaffoldBackgroundColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8), side: BorderSide(color: AppColors.primaryColor)),
                        padding: EdgeInsets.symmetric(vertical: 16),
                        onTap: () {},
                        radius: 8,
                      ),
                    ),
                    SizedBox(width: 24),
                    Expanded(
                      child: AppButton(
                        text: Text("Successful", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Theme.of(context).scaffoldBackgroundColor)),
                        background: AppColors.primaryColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8), side: BorderSide(color: AppColors.primaryColor)),
                        padding: EdgeInsets.symmetric(vertical: 16),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (_) => SuccessDeliveryScreen(delivery: widget.delivery)));
                        },
                        radius: 8,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
