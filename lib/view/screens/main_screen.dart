import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/view/colors.dart';
import 'package:oech/view/screens/profile_screen.dart';
import 'package:oech/view/screens/track_screen.dart';
import 'package:oech/view/screens/wallet_screen.dart';

import 'home_screen.dart';

/*
Описание: Класс центральных экранов приложения
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class MainScreen extends StatefulWidget {
  final int? screen;

  const MainScreen({super.key, this.screen});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

/*
Описание: State Класса центральных экранов приложения
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _MainScreenState extends State<MainScreen> {
  List<Widget> screens = [
    HomeScreen(),
    WalletScreen(),
    TrackScreen(),
    ProfileScreen(),
  ];

  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();

    if (widget.screen != null) {
      _currentIndex = widget.screen!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: AppColors.primaryColor,
        unselectedItemColor: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
        selectedFontSize: 12,
        unselectedFontSize: 12,
        type: BottomNavigationBarType.fixed,
        onTap: (value) {
          setState(() {
            _currentIndex = value;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              _currentIndex == 0 ? "assets/icons/ic_home.svg" : "assets/icons/ic_home_2.svg",
              color: _currentIndex == 0
                  ? null
                  : Theme.of(context).brightness == Brightness.light
                      ? AppColors.grayColor
                      : Colors.white,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              _currentIndex == 1 ? "assets/icons/ic_wallet.svg" : "assets/icons/ic_wallet.svg",
              color: _currentIndex == 1
                  ? null
                  : Theme.of(context).brightness == Brightness.light
                      ? AppColors.grayColor
                      : Colors.white,
            ),
            label: "Wallet",
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              _currentIndex == 2 ? "assets/icons/ic_track.svg" : "assets/icons/ic_track_2.svg",
              color: _currentIndex == 2
                  ? null
                  : Theme.of(context).brightness == Brightness.light
                      ? AppColors.grayColor
                      : Colors.white,
            ),
            label: "Track",
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              _currentIndex == 3 ? "assets/icons/ic_profile.svg" : "assets/icons/ic_profile_2.svg",
              color: _currentIndex == 3
                  ? null
                  : Theme.of(context).brightness == Brightness.light
                      ? AppColors.grayColor
                      : Colors.white,
            ),
            label: "Profile",
          ),
        ],
      ),
      body: screens[_currentIndex],
    );
  }
}
