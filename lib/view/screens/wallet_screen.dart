import 'package:flutter/material.dart';
import 'package:oech/viewmodel/home_viewmodel.dart';
import 'package:provider/provider.dart';

import '../colors.dart';
import '../widgets/user_card.dart';
import '../widgets/wallet_card.dart';

/*
Описание: Класс экрана кошелька
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class WalletScreen extends StatefulWidget {
  const WalletScreen({super.key});

  @override
  State<WalletScreen> createState() => _WalletScreenState();
}

/*
Описание: State Класса экрана кошелька
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class _WalletScreenState extends State<WalletScreen> {
  late HomeViewModel viewModel;

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
          automaticallyImplyLeading: false,
          toolbarHeight: 60,
          title: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: const Text(
              "Wallet",
              style: TextStyle(fontSize: 16, color: AppColors.grayColor),
            ),
          ),
          backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              SizedBox(height: 36),
              UserCard(),
              SizedBox(height: 45),
              WalletCard(),
              SizedBox(height: 45),
              Text(
                "Transaction History",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
              ),
              SizedBox(height: 24),
              Expanded(
                child: FutureBuilder(
                    future: viewModel.getTransactions(),
                    builder: (builder, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                            itemCount: viewModel.transactions.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: EdgeInsets.symmetric(vertical: 6),
                                child: TransactionCard(
                                  title: viewModel.transactions[index].title,
                                  date: viewModel.transactions[index].date,
                                  sum: viewModel.transactions[index].sum,
                                  type: viewModel.transactions[index].type,
                                ),
                              );
                            });
                      } else {
                        return const Center(child: CircularProgressIndicator());
                      }
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
