import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/view/screens/rider_profile.dart';
import 'package:oech/view/widgets/app_text_field.dart';
import 'package:oech/viewmodel/home_viewmodel.dart';
import 'package:provider/provider.dart';

import '../../model/rider.dart';
import '../colors.dart';
import '../widgets/rider_card.dart';

/*
Описание: Экран списка доставщиков
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class BookRiderScreen extends StatefulWidget {
  const BookRiderScreen({super.key});

  @override
  State<BookRiderScreen> createState() => _BookRiderScreenState();
}

/*
Описание: State Экрана списка доставщиков
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class _BookRiderScreenState extends State<BookRiderScreen> {
  late HomeViewModel viewModel;

  final TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset("assets/icons/ic_back.svg"),
          ),
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
          automaticallyImplyLeading: false,
          toolbarHeight: 60,
          title: Text(
            "Book a Rider",
            style: TextStyle(fontSize: 16, color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              SizedBox(height: 24),
              S5TextField(
                controller: _searchController,
                onChanged: (value) {
                  setState(() {});
                },
                placeholder: "Search for a driver",
                padding: EdgeInsets.all(12),
                icon: SvgPicture.asset(
                  "assets/icons/ic_search.svg",
                  color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                ),
              ),
              SizedBox(height: 24),
              Expanded(
                child: FutureBuilder(
                  future: viewModel.getRiders(),
                  builder: (builder, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                          shrinkWrap: true,
                          itemCount: viewModel.riderList
                              .where(
                                  (element) => element.fullName.toLowerCase().contains(_searchController.text.toLowerCase()) || _searchController.text.isEmpty)
                              .length,
                          itemBuilder: (itemBuilder, index) {
                            List<Rider> sortedList = [];

                            sortedList.addAll(viewModel.riderList
                                .where((element) =>
                                    element.fullName.toLowerCase().contains(_searchController.text.toLowerCase()) || _searchController.text.isEmpty)
                                .toList());

                            return Column(
                              children: [
                                RiderCard(
                                  rider: sortedList[index],
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (_) => RiderProfileScreen(rider: sortedList[index])));
                                  },
                                ),
                                SizedBox(height: 16),
                              ],
                            );
                          });
                    } else {
                      return const Center(child: CircularProgressIndicator());
                    }
                  },
                ),
              ),
              SizedBox(height: 60),
            ],
          ),
        ),
      ),
    );
  }
}
