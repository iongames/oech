import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../colors.dart';

/*
Описание: Класс экрана уведомлений
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class NotificationScreen extends StatefulWidget {
  const NotificationScreen({super.key});

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

/*
Описание: State Класса экрана уведомлений
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset("assets/icons/ic_back.svg"),
          ),
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
          automaticallyImplyLeading: false,
          toolbarHeight: 60,
          title: const Text(
            "Notification",
            style: TextStyle(fontSize: 16, color: AppColors.grayColor),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
        ),
        body: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    SizedBox(height: 120),
                    SvgPicture.asset(
                      "assets/icons/ic_notification.svg",
                      width: 83,
                      height: 83,
                      color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                    ),
                    SizedBox(height: 18),
                    Text(
                      "You have no notifications",
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
