import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oech/model/delivery.dart';
import 'package:oech/view/colors.dart';
import 'package:oech/view/screens/main_screen.dart';

import '../widgets/app_button.dart';

/*
Описание: Экран успешной оплаты доставки
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class SuccessScreen extends StatefulWidget {
  final Delivery delivery;

  const SuccessScreen({super.key, required this.delivery});

  @override
  State<SuccessScreen> createState() => _SuccessScreenState();
}

/*
Описание: State экрана успешной оплаты доставки
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class _SuccessScreenState extends State<SuccessScreen> with TickerProviderStateMixin {
  late AnimationController aController = AnimationController(
    vsync: this,
    duration: Duration(
      seconds: 1,
    ),
  )..repeat();

  String imageAsset = "assets/images/loading.png";
  bool loaded = false;

  @override
  void initState() {
    super.initState();

    Timer timer = Timer.periodic(Duration(seconds: 2), (timer) {
      aController.stop();
      timer.cancel();

      setState(() {
        imageAsset = "assets/images/success.png";
        loaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(),
            Column(
              children: [
                AnimatedBuilder(
                  animation: aController,
                  builder: (builder, _child) {
                    return Transform.rotate(
                      angle: aController.value * -6,
                      child: _child,
                    );
                  },
                  child: Image.asset(
                    imageAsset,
                    width: 119,
                    height: 119,
                  ),
                ),
                SizedBox(height: 75),
                loaded
                    ? Text(
                        "Transaction Successful",
                        style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                      )
                    : SizedBox(),
                loaded ? SizedBox(height: 8) : SizedBox(),
                Text(
                  "Your rider is on the way to your destination",
                  style: TextStyle(fontSize: 14, color: Theme.of(context).hintColor),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 4),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "Tracking Number ",
                        style: TextStyle(fontSize: 14, color: Theme.of(context).hintColor),
                      ),
                      TextSpan(
                        text: widget.delivery.trackingNumber,
                        style: TextStyle(fontSize: 14, color: AppColors.primaryColor),
                      ),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(),
            Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: AppButton(
                        text: Text("Track my item",
                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Theme.of(context).scaffoldBackgroundColor)),
                        background: AppColors.primaryColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8), side: BorderSide(color: AppColors.primaryColor)),
                        padding: EdgeInsets.symmetric(vertical: 15),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (_) => const MainScreen(screen: 2)));
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  children: [
                    Expanded(
                      child: AppButton(
                        text: const Text("Go back to homepage", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: AppColors.primaryColor)),
                        background: Theme.of(context).scaffoldBackgroundColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8), side: BorderSide(color: AppColors.primaryColor)),
                        padding: EdgeInsets.symmetric(vertical: 15),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (_) => const MainScreen()));
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox()
          ],
        ),
      ),
    ));
  }
}
