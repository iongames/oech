import 'package:flutter/material.dart';
import 'package:oech/view/screens/main_screen.dart';
import 'package:provider/provider.dart';

import '../../viewmodel/login_viewmodel.dart';
import '../colors.dart';
import '../widgets/app_alert_dialog.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';

/*
Описание: Класс экрана создания пароля
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class NewPasswordScreen extends StatefulWidget {
  const NewPasswordScreen({super.key});

  @override
  State<NewPasswordScreen> createState() => _NewPasswordScreenState();
}

/*
Описание: State Класса экрана создания пароля
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _NewPasswordScreenState extends State<NewPasswordScreen> {
  late LoginViewModel viewModel;

  final TextEditingController _p1Controller = TextEditingController();
  final TextEditingController _p2Controller = TextEditingController();

  /*
  Описание: Метод инициализации класса
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 110),
                  Text(
                    "New Password",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Enter new password",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 48),
                  Text(
                    "New Password",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    placeholder: "********",
                    controller: _p1Controller,
                    obscureText: true,
                  ),
                  SizedBox(height: 24),
                  Text(
                    "Confirm Password",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    placeholder: "********",
                    controller: _p2Controller,
                    obscureText: true,
                  ),
                  SizedBox(height: 93),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: AppButton(
                        text: Text(
                          "Log in",
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),
                        ),
                        onTap: () async {
                          if (_p1Controller.text == _p2Controller.text) {
                            showDialog(
                              context: context,
                              builder: (builder) {
                                return const Center(child: CircularProgressIndicator());
                              },
                            );

                            bool result = await viewModel.setNewPassword(_p1Controller.text);

                            if (result) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => const MainScreen(),
                                ),
                              );
                            }
                          } else {
                            showDialog(
                              context: context,
                              builder: (builder) {
                                return AppAlertDialog(title: "ERROR", content: "Пароли не совпадают!");
                              },
                            );
                          }
                        },
                      ))
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
