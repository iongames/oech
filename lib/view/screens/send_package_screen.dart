import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/model/delivery.dart';
import 'package:oech/model/send_info.dart';
import 'package:oech/view/screens/package_info.dart';
import 'package:oech/view/widgets/app_text_field.dart';
import 'package:oech/viewmodel/home_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

import '../colors.dart';

/*
Описание: Класс экрана отправки товаров
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class SendPackageScreen extends StatefulWidget {
  const SendPackageScreen({super.key});

  @override
  State<SendPackageScreen> createState() => _SendPackageScreenState();
}

/*
Описание: State Класса экрана отправки товаров
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class _SendPackageScreenState extends State<SendPackageScreen> {
  late HomeViewModel viewModel;

  final TextEditingController sAddressController = TextEditingController();
  final TextEditingController sStateController = TextEditingController();
  final TextEditingController sPhoneController = TextEditingController();
  final TextEditingController sOtherController = TextEditingController();

  final TextEditingController dAddressController = TextEditingController();
  final TextEditingController dStateController = TextEditingController();
  final TextEditingController dPhoneController = TextEditingController();
  final TextEditingController dOtherController = TextEditingController();

  final List<SendInfo> destInfoList = [];

  final TextEditingController sItemsController = TextEditingController();
  final TextEditingController sWeightController = TextEditingController();
  final TextEditingController sWorthController = TextEditingController();

  int count = 0;

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset("assets/icons/ic_back.svg"),
          ),
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
          automaticallyImplyLeading: false,
          toolbarHeight: 60,
          title: const Text(
            "Send a package",
            style: TextStyle(fontSize: 16, color: AppColors.grayColor),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
        ),
        body: FutureBuilder(
            future: viewModel.getCurrentLocation(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data == true) {
                  sAddressController.text = viewModel.name;
                }

                return SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 43),
                        Row(
                          children: [
                            SvgPicture.asset(
                              "assets/icons/ic_start.svg",
                            ),
                            SizedBox(width: 8),
                            Text(
                              "Origin Details",
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: sAddressController,
                          placeholder: "Address",
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: sStateController,
                          placeholder: "State,Country",
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: sPhoneController,
                          placeholder: "Phone number",
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: sOtherController,
                          placeholder: "Others",
                        ),
                        SizedBox(height: 39),
                        Row(
                          children: [
                            SvgPicture.asset(
                              "assets/icons/ic_dest.svg",
                            ),
                            SizedBox(width: 8),
                            Text(
                              "Destination Details",
                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                            ),
                          ],
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: dAddressController,
                          placeholder: "Address",
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: dStateController,
                          placeholder: "State,Country",
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: dPhoneController,
                          placeholder: "Phone number",
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: dOtherController,
                          placeholder: "Others",
                        ),
                        Column(
                          children: List.generate(count, (index) {
                            final TextEditingController dNAddressController = TextEditingController();
                            final TextEditingController dNStateController = TextEditingController();
                            final TextEditingController dNPhoneController = TextEditingController();
                            final TextEditingController dNOtherController = TextEditingController();

                            return Column(
                              children: [
                                SizedBox(height: 24),
                                Row(
                                  children: [
                                    SvgPicture.asset(
                                      "assets/icons/ic_dest.svg",
                                    ),
                                    SizedBox(width: 8),
                                    Text(
                                      "Destination Details",
                                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 5),
                                DeliveryTextField(
                                  controller: dNAddressController,
                                  placeholder: "Address",
                                  onChanged: (value) {
                                    destInfoList[index].address = dNAddressController.text;
                                  },
                                ),
                                SizedBox(height: 5),
                                DeliveryTextField(
                                  controller: dNStateController,
                                  placeholder: "State,Country",
                                  onChanged: (value) {
                                    destInfoList[index].state = dNStateController.text;
                                  },
                                ),
                                SizedBox(height: 5),
                                DeliveryTextField(
                                  controller: dNPhoneController,
                                  placeholder: "Phone number",
                                  onChanged: (value) {
                                    destInfoList[index].phone = dNPhoneController.text;
                                  },
                                ),
                                SizedBox(height: 5),
                                DeliveryTextField(
                                  controller: dNOtherController,
                                  placeholder: "Others",
                                  onChanged: (value) {
                                    destInfoList[index].other = dNOtherController.text;
                                  },
                                ),
                              ],
                            );
                          }),
                        ),
                        SizedBox(height: 10),
                        InkWell(
                          onTap: () {
                            setState(() {
                              count++;

                              destInfoList.add(SendInfo("", "", "", ""));
                            });
                          },
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                "assets/icons/ic_add.svg",
                              ),
                              SizedBox(width: 8),
                              Text(
                                "Add destination",
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 13),
                        Text(
                          "Package Details",
                          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                        ),
                        SizedBox(height: 8),
                        DeliveryTextField(
                          controller: sItemsController,
                          placeholder: "package items",
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: sWeightController,
                          placeholder: "Weight of item(kg)",
                        ),
                        SizedBox(height: 5),
                        DeliveryTextField(
                          controller: sWorthController,
                          placeholder: "Worth of Items",
                        ),
                        SizedBox(height: 39),
                        Text(
                          "Select delivery type",
                          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                        ),
                        SizedBox(height: 16),
                        Row(
                          children: [
                            Expanded(
                              child: Card(
                                elevation: 3,
                                color: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
                                shadowColor: Colors.black.withOpacity(0.5),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                clipBehavior: Clip.antiAlias,
                                child: InkWell(
                                  onTap: () async {
                                    Delivery delivery = Delivery(
                                      SendInfo(
                                        sAddressController.text,
                                        sStateController.text,
                                        sPhoneController.text,
                                        sOtherController.text,
                                      ),
                                      List.from({
                                        SendInfo(dAddressController.text, dStateController.text, dPhoneController.text, dOtherController.text),
                                      })
                                        ..addAll(destInfoList),
                                      sItemsController.text,
                                      sWeightController.text,
                                      sWorthController.text,
                                      "R-${const Uuid().v4()}",
                                    );

                                    showDialog(
                                      context: context,
                                      builder: (builder) {
                                        return const Center(child: CircularProgressIndicator());
                                      },
                                    );

                                    bool result = await viewModel.createDelivery(delivery);

                                    if (result) {
                                      Navigator.push(context, MaterialPageRoute(builder: (_) => PackageInfoScreen(delivery: delivery)));
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 12),
                                    child: Column(
                                      children: [
                                        SvgPicture.asset(
                                          "assets/icons/ic_clock.svg",
                                          color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                                        ),
                                        SizedBox(height: 10),
                                        Text(
                                          "Instant delivery",
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 24),
                            Expanded(
                              child: Card(
                                elevation: 3,
                                color: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
                                shadowColor: Colors.black.withOpacity(0.5),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                clipBehavior: Clip.antiAlias,
                                child: InkWell(
                                  onTap: () {},
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 12),
                                    child: Column(
                                      children: [
                                        SvgPicture.asset(
                                          "assets/icons/ic_calendar.svg",
                                          color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                                        ),
                                        SizedBox(height: 10),
                                        Text(
                                          "Scheduled delivery",
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 85),
                      ],
                    ),
                  ),
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }),
      ),
    );
  }
}
