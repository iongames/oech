import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_osm_plugin/flutter_osm_plugin.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:oech/view/colors.dart';
import 'package:oech/view/screens/package_deliveried.dart';
import 'package:oech/view/screens/package_info.dart';
import 'package:oech/view/widgets/app_button.dart';
import 'package:oech/viewmodel/home_viewmodel.dart';
import 'package:provider/provider.dart';

/*
Описание: Класс экрана отслеживания заказа
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class TrackScreen extends StatefulWidget {
  const TrackScreen({super.key});

  @override
  State<TrackScreen> createState() => _TrackScreenState();
}

/*
Описание: State Класса экрана отслеживания заказа
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class _TrackScreenState extends State<TrackScreen> {
  late HomeViewModel viewModel;

  final MapController _mapController = MapController(initPosition: GeoPoint(latitude: 0, longitude: 0));

  bool _value1 = true;
  bool _value2 = true;
  bool _value3 = false;
  bool _value4 = false;

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: viewModel.loadLastDelivery(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data == true) {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        height: 320,
                        child: OSMFlutter(
                          controller: _mapController,
                          enableRotationByGesture: true,
                          staticPoints: [],
                        ),
                      ),
                      const SizedBox(height: 42),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Tracking Number",
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                            ),
                            SizedBox(height: 24),
                            Row(
                              children: [
                                SvgPicture.asset("assets/icons/ic_sun.svg"),
                                SizedBox(width: 8),
                                Text(viewModel.delivery!.trackingNumber),
                              ],
                            ),
                            SizedBox(height: 16),
                            Text(
                              "Package Status",
                              style: TextStyle(fontSize: 14, color: AppColors.grayColor),
                            ),
                            SizedBox(height: 24),
                            TrackField(
                              value: _value1,
                              title: "Courier requested",
                              date: DateTime.now(),
                              hasLine: true,
                            ),
                            TrackField(
                              value: _value2,
                              title: "Package ready for delivery",
                              date: DateTime.now(),
                              hasLine: true,
                            ),
                            TrackField(
                              value: _value3,
                              title: "Package in transit",
                              date: DateTime.now(),
                              hasLine: true,
                            ),
                            TrackField(
                              value: _value4,
                              title: "Package delivered",
                              date: DateTime.now(),
                              hasLine: false,
                            ),
                            SizedBox(height: 40),
                            AppButton(
                                text: Text(
                                  "View Package Info",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (_) => PackagedDeliveriedScreen(delivery: viewModel.delivery!)));
                                })
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                return const Center(child: CircularProgressIndicator());
              }
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          }),
    );
  }
}

/*
Описание: Поле отслеживания заказа
Дата создания: 03.07.2023
Автор: Хасанов Георгий
*/
class TrackField extends StatelessWidget {
  final String title;
  final DateTime date;
  final bool value;
  final bool hasLine;

  const TrackField({
    super.key,
    required this.value,
    required this.title,
    required this.date,
    required this.hasLine,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      hasLine
          ? Positioned(
              top: 0,
              bottom: 0,
              left: 6,
              child: Container(
                width: 1,
                color: AppColors.grayColor,
              ),
            )
          : SizedBox(),
      Padding(
        padding: const EdgeInsets.only(bottom: 12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 14,
              height: 14,
              color: Theme.of(context).scaffoldBackgroundColor,
              child: Checkbox(
                value: value,
                onChanged: (v) {},
                activeColor: AppColors.primaryColor,
                side: BorderSide(width: 2, color: AppColors.primaryColor),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.33)),
              ),
            ),
            SizedBox(width: 7),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 14, color: value ? AppColors.primaryColor : AppColors.grayColor),
                ),
                SizedBox(height: 4),
                Text(
                  DateFormat("MMM dd, yyyy h:mm a").format(date),
                  style: TextStyle(fontSize: 14, color: value ? AppColors.secondaryColor : AppColors.grayColor),
                ),
              ],
            ),
          ],
        ),
      ),
    ]);
  }
}
