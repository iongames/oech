import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/model/delivery.dart';
import 'package:oech/view/colors.dart';
import 'package:oech/view/screens/main_screen.dart';
import 'package:oech/view/widgets/app_text_field.dart';

import '../widgets/app_button.dart';

/*
Описание: Экран успешной доставки
Дата создания: 03.07.2023
Автор: Хасанов Георгий
*/
class SuccessDeliveryScreen extends StatefulWidget {
  final Delivery delivery;

  const SuccessDeliveryScreen({super.key, required this.delivery});

  @override
  State<SuccessDeliveryScreen> createState() => _SuccessDeliveryScreenState();
}

/*
Описание: State экрана успешной доставки
Дата создания: 03.07.2023
Автор: Хасанов Георгий
*/
class _SuccessDeliveryScreenState extends State<SuccessDeliveryScreen> with TickerProviderStateMixin {
  late AnimationController aController = AnimationController(
    vsync: this,
    duration: Duration(
      seconds: 1,
    ),
  )..repeat();

  String imageAsset = "assets/images/loading.png";
  bool loaded = false;

  @override
  void initState() {
    super.initState();

    Timer timer = Timer.periodic(Duration(seconds: 2), (timer) {
      aController.stop();
      timer.cancel();

      setState(() {
        imageAsset = "assets/images/success.png";
        loaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              SizedBox(height: 111),
              Column(
                children: [
                  AnimatedBuilder(
                    animation: aController,
                    builder: (builder, _child) {
                      return Transform.rotate(
                        angle: aController.value * -6,
                        child: _child,
                      );
                    },
                    child: Image.asset(
                      imageAsset,
                      width: 119,
                      height: 119,
                    ),
                  ),
                  SizedBox(height: 75),
                  loaded
                      ? Text(
                          "Delivery Successful",
                          style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                        )
                      : SizedBox(),
                  loaded ? SizedBox(height: 8) : SizedBox(),
                  loaded
                      ? Text(
                          "Your Item has been delivered successfully",
                          style: TextStyle(fontSize: 14, color: Theme.of(context).hintColor),
                          textAlign: TextAlign.center,
                        )
                      : SizedBox(),
                  SizedBox(height: 67),
                  Text(
                    "Rate Rider",
                    style: TextStyle(fontSize: 14, color: AppColors.primaryColor),
                  ),
                  SizedBox(height: 16),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets/icons/ic_star.svg",
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                      SizedBox(width: 16),
                      SvgPicture.asset(
                        "assets/icons/ic_star.svg",
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                      SizedBox(width: 16),
                      SvgPicture.asset(
                        "assets/icons/ic_star.svg",
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                      SizedBox(width: 16),
                      SvgPicture.asset(
                        "assets/icons/ic_star.svg",
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                      SizedBox(width: 16),
                      SvgPicture.asset(
                        "assets/icons/ic_star.svg",
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ],
                  ),
                  SizedBox(height: 36),
                  RateTextField(placeholder: "Add feedback"),
                  SizedBox(height: 76),
                ],
              ),
              SizedBox(),
              Row(
                children: [
                  Expanded(
                    child: AppButton(
                      text: Text("Done", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white)),
                      background: AppColors.primaryColor,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8), side: BorderSide(color: AppColors.primaryColor)),
                      padding: EdgeInsets.symmetric(vertical: 16),
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) => MainScreen()));
                      },
                    ),
                  ),
                ],
              ),
              SizedBox()
            ],
          ),
        ),
      ),
    ));
  }
}
