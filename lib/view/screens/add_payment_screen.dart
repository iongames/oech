import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/view/widgets/app_button.dart';

import '../../model/payment.dart';
import '../colors.dart';

/*
Описание: Класс экрана добавления оплаты
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class AddPaymentScreen extends StatefulWidget {
  const AddPaymentScreen({super.key});

  @override
  State<AddPaymentScreen> createState() => _AddPaymentScreenState();
}

/*
Описание: State Класса экрана добавления оплаты
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class _AddPaymentScreenState extends State<AddPaymentScreen> {
  List<Payment> paymentList = [
    Payment("**** **** 3323"),
  ];

  int? _selectedIndex;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset("assets/icons/ic_back.svg"),
          ),
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
          automaticallyImplyLeading: false,
          toolbarHeight: 60,
          title: const Text(
            "Add payment method",
            style: TextStyle(fontSize: 16, color: AppColors.grayColor),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
        ),
        body: Column(
          children: [
            Expanded(
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: Column(
                        children: [
                          SizedBox(height: 67),
                          PaymentCard(
                            title: "Pay with wallet",
                            desc: "complete the payment using your e wallet",
                            onTap: () {},
                          ),
                          SizedBox(height: 12),
                          PaymentCard(
                            title: "Credit / debit card",
                            desc: "add new card",
                            onTap: () {},
                          ),
                          SizedBox(height: 12),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: List.generate(1, (index) {
                              return PaymentCard(
                                title: paymentList[index].title,
                                desc: "remove card",
                                index: index,
                                selectedIndex: _selectedIndex,
                                onTap: () {
                                  setState(() {
                                    _selectedIndex = index;
                                  });
                                },
                              );
                            }),
                          )
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 105,
                    left: 24,
                    right: 24,
                    child: AppButton(
                      text: Text(
                        "Proceed to pay",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color: _selectedIndex != null ? Theme.of(context).scaffoldBackgroundColor : Colors.white),
                      ),
                      onTap: () {},
                      padding: EdgeInsets.symmetric(vertical: 15),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/*
Описание: Карточка добавления способа оплаты
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class PaymentCard extends StatelessWidget {
  final String title;
  final String desc;
  final int? index;
  final int? selectedIndex;
  final Function() onTap;

  const PaymentCard({
    super.key,
    required this.title,
    required this.desc,
    required this.onTap,
    this.index = -1,
    this.selectedIndex,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
      clipBehavior: Clip.antiAlias,
      color: index == selectedIndex
          ? Theme.of(context).brightness == Brightness.light
              ? AppColors.lPrimarySelectColor
              : AppColors.dPrimarySelectColor
          : Theme.of(context).brightness == Brightness.light
              ? Colors.white
              : AppColors.dCardColor,
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 24),
          child: Row(
            children: [
              SvgPicture.asset("assets/icons/ic_check.svg"),
              SizedBox(width: 8),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(fontSize: 16, color: Theme.of(context).hintColor),
                  ),
                  Text(
                    desc,
                    style: TextStyle(
                      fontSize: 12,
                      color: AppColors.grayColor,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
