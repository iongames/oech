import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oech/main.dart';
import 'package:oech/view/colors.dart';
import 'package:oech/view/screens/add_payment_screen.dart';
import 'package:oech/view/screens/login_screen.dart';
import 'package:oech/view/screens/notification_screen.dart';
import 'package:oech/view/widgets/profile_card.dart';
import 'package:oech/view/widgets/user_card.dart';

/*
Описание: Класс экрана профиля
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

/*
Описание: State Класса экрана профиля
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
          automaticallyImplyLeading: false,
          toolbarHeight: 60,
          title: const Text(
            "Profile",
            style: TextStyle(fontSize: 16, color: AppColors.grayColor),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              children: [
                SizedBox(height: 36),
                UserCard(),
                SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Enable dark Mode",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).hintColor,
                      ),
                    ),
                    CupertinoSwitch(
                        value: Theme.of(context).brightness == Brightness.dark,
                        activeColor: AppColors.primaryColor,
                        onChanged: (value) {
                          if (Theme.of(context).brightness == Brightness.dark) {
                            Get.changeThemeMode(ThemeMode.light);
                          } else {
                            Get.changeThemeMode(ThemeMode.dark);
                          }
                        })
                  ],
                ),
                SizedBox(height: 19),
                ProfileCard(
                  title: "Edit Profile",
                  content: "Name, phone no, address, email ...",
                  icon: "ic_profile_2",
                  onTap: () {},
                ),
                SizedBox(height: 12),
                ProfileCard(
                  title: "Statements & Reports",
                  content: "Download transaction details, orders, deliveries",
                  icon: "ic_cert",
                  onTap: () {},
                ),
                SizedBox(height: 12),
                ProfileCard(
                  title: "Notification Settings",
                  content: "mute, unmute, set location & tracking setting",
                  icon: "ic_notification",
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => const NotificationScreen()));
                  },
                ),
                SizedBox(height: 12),
                ProfileCard(
                  title: "Card & Bank account settings",
                  content: "change cards, delete card details",
                  icon: "ic_wallet",
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => const AddPaymentScreen()));
                  },
                ),
                SizedBox(height: 12),
                ProfileCard(
                  title: "Referrals",
                  content: "check no of friends and earn",
                  icon: "ic_ref",
                  onTap: () {},
                ),
                SizedBox(height: 12),
                ProfileCard(
                  title: "About Us",
                  content: "know more about us, terms and conditions",
                  icon: "ic_about",
                  onTap: () {},
                ),
                SizedBox(height: 12),
                LogoutCard(
                  onLogout: () async {
                    await supabase.auth.signOut();

                    Navigator.push(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
