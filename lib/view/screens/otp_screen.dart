import 'package:flutter/material.dart';
import 'package:oech/view/widgets/app_text_field.dart';
import 'package:oech/viewmodel/login_viewmodel.dart';
import 'package:provider/provider.dart';

import '../colors.dart';
import '../widgets/app_button.dart';
import 'new_password_screen.dart';

/*
Описание: Класс экрана восстановления пароля
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class OTPScreen extends StatefulWidget {
  final String email;

  const OTPScreen({super.key, required this.email});

  @override
  State<OTPScreen> createState() => _OTPScreenState();
}

/*
Описание: State Класса экрана восстановления пароля
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _OTPScreenState extends State<OTPScreen> {
  late LoginViewModel viewModel;

  final TextEditingController _code1 = TextEditingController();
  final TextEditingController _code2 = TextEditingController();
  final TextEditingController _code3 = TextEditingController();
  final TextEditingController _code4 = TextEditingController();
  final TextEditingController _code5 = TextEditingController();
  final TextEditingController _code6 = TextEditingController();

  /*
  Описание: Метод инициализации класса
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 76),
                  Text(
                    "OTP Verification",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Enter the 6 digit numbers sent to your email",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 70),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CodeTextField(controller: _code1),
                      CodeTextField(controller: _code2),
                      CodeTextField(controller: _code3),
                      CodeTextField(controller: _code4),
                      CodeTextField(controller: _code5),
                      CodeTextField(controller: _code6),
                    ],
                  ),
                  SizedBox(height: 12),
                  InkWell(
                    onTap: () async {
                      showDialog(
                        context: context,
                        builder: (builder) {
                          return const Center(child: CircularProgressIndicator());
                        },
                      );

                      bool result = await viewModel.forgotPassword(widget.email);
                    },
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "If you didn’t receive code, ",
                            style: TextStyle(fontSize: 14, color: AppColors.grayColor),
                          ),
                          TextSpan(
                            text: "Resend",
                            style: TextStyle(fontSize: 14, color: AppColors.primaryColor),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 84),
                  Row(
                    children: [
                      Expanded(
                          child: AppButton(
                        text: Text(
                          "Set New Password",
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),
                        ),
                        onTap: () async {
                          showDialog(
                            context: context,
                            builder: (builder) {
                              return const Center(child: CircularProgressIndicator());
                            },
                          );

                          bool result = await viewModel.checkCode(
                            widget.email,
                            "${_code1.text}${_code2.text}${_code3.text}${_code4.text}${_code5.text}${_code6.text}",
                          );

                          if (result) {
                            Navigator.push(context, MaterialPageRoute(builder: (_) => const NewPasswordScreen()));
                          }
                        },
                      ))
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
