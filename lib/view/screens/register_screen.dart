import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:oech/view/colors.dart';
import 'package:oech/view/screens/login_screen.dart';
import 'package:oech/view/widgets/app_button.dart';
import 'package:oech/view/widgets/app_text_field.dart';
import 'package:oech/viewmodel/login_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../widgets/app_alert_dialog.dart';

/*
Описание: Класс экрана регистрации
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

/*
Описание: State Класса экрана регистрации
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _RegisterScreenState extends State<RegisterScreen> {
  late LoginViewModel viewModel;

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _checked = false;
  bool _hide = true;

  /*
  Описание: Метод инициализации класса
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 36),
                  Text(
                    "Create an account",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Complete the sign up process to get started",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 30),
                  Text(
                    "Full Name",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    placeholder: "Abecd   fsgh",
                    controller: _nameController,
                  ),
                  SizedBox(height: 24),
                  Text(
                    "Phone Number",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    placeholder: "000000000000",
                    controller: _phoneController,
                  ),
                  SizedBox(height: 24),
                  Text(
                    "Email Address",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    placeholder: "***********@mail.com",
                    controller: _emailController,
                  ),
                  SizedBox(height: 24),
                  Text(
                    "Password",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    placeholder: "**********",
                    controller: _passwordController,
                    obscureText: _hide,
                    icon: InkWell(
                        onTap: () {
                          setState(() {
                            _hide = !_hide;
                          });
                        },
                        child: SvgPicture.asset("assets/icons/ic_eye.svg")),
                  ),
                  SizedBox(height: 8),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 14,
                        height: 14,
                        child: Checkbox(
                          value: _checked,
                          onChanged: (value) {
                            setState(() {
                              _checked = !_checked;
                            });
                          },
                          activeColor: AppColors.primaryColor,
                          side: BorderSide(width: 2, color: AppColors.primaryColor),
                        ),
                      ),
                      SizedBox(width: 11),
                      InkWell(
                        onTap: () {
                          launchUrl(Uri.parse("https://google.com"));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width - 110,
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: "By ticking this box, you agree to our ",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: AppColors.grayColor,
                                  ),
                                ),
                                TextSpan(
                                  text: "Terms and conditions and private policy",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: AppColors.warningColor,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 64),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: AppButton(
                        text: Text(
                          "Sign Up",
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),
                        ),
                        onTap: () async {
                          if (RegExp(r"^[a-z0-9]*@[a-z0-9]*\.[a-z0-9]*$").hasMatch(_emailController.text)) {
                            if (_checked) {
                              showDialog(
                                context: context,
                                builder: (builder) {
                                  return const Center(child: CircularProgressIndicator());
                                },
                              );

                              bool result =
                                  await viewModel.signUp(_emailController.text, _passwordController.text, _phoneController.text, _nameController.text);

                              if (result) {
                                Navigator.push(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
                              }
                            } else {
                              showDialog(
                                context: context,
                                builder: (builder) {
                                  return AppAlertDialog(title: "ERROR", content: "Необходимо принять соглашение!");
                                },
                              );
                            }
                          } else {
                            showDialog(
                              context: context,
                              builder: (builder) {
                                return AppAlertDialog(title: "ERROR", content: "Неправильный формат E-Mail!");
                              },
                            );
                          }
                        },
                      ))
                    ],
                  ),
                  SizedBox(height: 8),
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
                    },
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Already have an account? ",
                            style: TextStyle(fontSize: 14, color: AppColors.grayColor),
                          ),
                          TextSpan(
                            text: "Sign in",
                            style: TextStyle(fontSize: 14, color: AppColors.primaryColor),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 30),
                  Column(
                    children: [
                      Text(
                        "or sign in using",
                        style: TextStyle(
                          fontSize: 14,
                          color: AppColors.grayColor,
                        ),
                      ),
                      SizedBox(height: 8),
                      SvgPicture.asset("assets/icons/ic_google.svg"),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
