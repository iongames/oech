import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/view/screens/call_rider.dart';
import 'package:oech/view/widgets/app_text_field.dart';
import 'package:oech/viewmodel/home_viewmodel.dart';
import 'package:provider/provider.dart';

import '../../model/rider.dart';
import '../colors.dart';
import '../widgets/message_card.dart';

/*
Описание: Экран чата с доставщиком
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class ChatScreen extends StatefulWidget {
  final Rider rider;

  const ChatScreen({super.key, required this.rider});

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

/*
Описание: State Экрана чата с доставщиком
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class _ChatScreenState extends State<ChatScreen> {
  late HomeViewModel viewModel;

  final TextEditingController textController = TextEditingController();

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: SvgPicture.asset("assets/icons/ic_back.svg"),
              ),
              elevation: 5,
              shadowColor: Colors.black.withOpacity(0.5),
              automaticallyImplyLeading: false,
              toolbarHeight: 60,
              title: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(),
                    Row(
                      children: [
                        Card(
                          elevation: 0,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                          color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                          clipBehavior: Clip.antiAlias,
                          child: Padding(
                            padding: const EdgeInsets.all(2),
                            child: const SizedBox(height: 43, width: 43),
                          ),
                        ),
                        SizedBox(width: 8),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.rider.fullName,
                              style: TextStyle(fontSize: 14, color: Theme.of(context).hintColor),
                            ),
                            SizedBox(height: 3),
                            Text(
                              "Online", // widget.rider.status
                              style: TextStyle(fontSize: 12, color: AppColors.primaryColor),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(),
                    SizedBox(),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) => CallRiderScreen(rider: widget.rider)));
                      },
                      child: SvgPicture.asset("assets/icons/ic_phone.svg"),
                    )
                  ],
                ),
              ),
              centerTitle: true,
              backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
            ),
            body: Column(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24),
                          child: Column(
                            children: List.generate(viewModel.messageList.length, (index) {
                              return Column(
                                children: [
                                  index == 0 ? SizedBox(height: 30) : SizedBox(),
                                  MessageCard(message: viewModel.messageList[index]),
                                  SizedBox(height: 12),
                                ],
                              );
                            }),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 24,
                        right: 24,
                        child: Container(
                          color: Theme.of(context).scaffoldBackgroundColor,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 14),
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: () {},
                                  child: SvgPicture.asset(
                                    "assets/icons/ic_emoji.svg",
                                    color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                                  ),
                                ),
                                SizedBox(width: 7),
                                Expanded(
                                  child: S5TextField(
                                    controller: textController,
                                    onChanged: (p0) {
                                      setState(() {});
                                    },
                                    placeholder: "Enter message",
                                    padding: EdgeInsets.symmetric(horizontal: 32, vertical: 12),
                                    icon: SvgPicture.asset(
                                      "assets/icons/ic_micro.svg",
                                      color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 1),
                                InkWell(
                                  onTap: () {},
                                  child: SvgPicture.asset(
                                      Theme.of(context).brightness == Brightness.light ? "assets/icons/ic_triangle.svg" : "assets/icons/ic_triangle_d.svg"),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )));
  }
}
