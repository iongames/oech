import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../model/rider.dart';
import '../colors.dart';

/*
Описание: Экран чата звонка доставщику
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class CallRiderScreen extends StatefulWidget {
  final Rider rider;

  const CallRiderScreen({super.key, required this.rider});

  @override
  State<CallRiderScreen> createState() => _CallRiderScreenState();
}

/*
Описание: State Экрана звонка с доставщику
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class _CallRiderScreenState extends State<CallRiderScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(),
                  Column(
                    children: [
                      Card(
                        elevation: 0,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                        clipBehavior: Clip.antiAlias,
                        child: Padding(
                          padding: const EdgeInsets.all(2),
                          child: const SizedBox(height: 84, width: 84),
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        widget.rider.fullName,
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w700,
                          color: AppColors.primaryColor,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        widget.rider.phoneNumber,
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w700,
                          color: AppColors.grayColor,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        "calling...",
                        style: TextStyle(
                          fontSize: 14,
                          color: AppColors.primaryColor,
                        ),
                      ),
                    ],
                  ),
                  Card(
                    elevation: 0,
                    margin: EdgeInsets.zero,
                    color: Theme.of(context).brightness == Brightness.light ? Color(0xFFF2F2F2) : AppColors.dCardColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                    clipBehavior: Clip.antiAlias,
                    child: Padding(
                      padding: EdgeInsets.all(50),
                      child: Column(
                        children: [
                          SvgPicture.asset(
                            "assets/images/buttons.svg",
                            color: Theme.of(context).hintColor,
                          ),
                          SizedBox(height: 56),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: SvgPicture.asset(
                              "assets/icons/ic_call.svg",
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
