import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/model/review.dart';
import 'package:oech/model/rider.dart';
import 'package:oech/view/screens/call_rider.dart';
import 'package:oech/view/widgets/rider_card.dart';

import '../colors.dart';
import '../widgets/app_button.dart';
import 'chat_screen.dart';

/*
Описание: Экран профиля доставщика
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class RiderProfileScreen extends StatefulWidget {
  final Rider rider;

  const RiderProfileScreen({super.key, required this.rider});

  @override
  State<RiderProfileScreen> createState() => _RiderProfileScreenState();
}

/*
Описание: State Экрана профиля доставщика
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class _RiderProfileScreenState extends State<RiderProfileScreen> {
  final List<Review> reviewList = [
    Review("Mrs Bimbo", "Right on time", 4.0),
    Review("Gentle Jack", "Calls with update on location", 5.0),
  ];

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: SvgPicture.asset("assets/icons/ic_back.svg"),
        ),
        elevation: 5,
        shadowColor: Colors.black.withOpacity(0.5),
        automaticallyImplyLeading: false,
        toolbarHeight: 60,
        title: Text(
          "Rider profile",
          style: TextStyle(fontSize: 16, color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
      ),
      body: SingleChildScrollView(
        child: Stack(children: [
          Container(
            height: 212,
            child: Card(
              elevation: 0,
              margin: EdgeInsets.zero,
              color: Colors.transparent,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16), bottomRight: Radius.circular(16))),
              clipBehavior: Clip.antiAlias,
              child: Image.asset(
                Theme.of(context).brightness == Brightness.light ? "assets/images/map_light.png" : "assets/images/map_night.png",
                fit: BoxFit.cover,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.bottomCenter,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 156),
                    Card(
                      elevation: 0,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                      color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      clipBehavior: Clip.antiAlias,
                      child: Padding(
                        padding: const EdgeInsets.all(2),
                        child: const SizedBox(height: 84, width: 84),
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      widget.rider.fullName,
                      style: TextStyle(
                        fontSize: 19,
                        fontWeight: FontWeight.w700,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.primaryColor : Colors.white,
                      ),
                    ),
                    SizedBox(height: 9),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(5, (index) {
                        return Row(
                          children: [
                            SvgPicture.asset(
                              "assets/icons/ic_star.svg",
                              width: 9,
                              height: 9,
                              color: index <= widget.rider.rating
                                  ? Theme.of(context).brightness == Brightness.light
                                      ? AppColors.primaryColor
                                      : Colors.white
                                  : Theme.of(context).brightness == Brightness.light
                                      ? AppColors.grayColor
                                      : Colors.white,
                            ),
                            SizedBox(width: 8)
                          ],
                        );
                      }),
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Text(
                                "Car Model",
                                style: TextStyle(fontSize: 12, color: AppColors.secondaryColor),
                              ),
                              SizedBox(height: 8),
                              Text(
                                widget.rider.carModel,
                                style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Text(
                                "Registration Number",
                                style: TextStyle(fontSize: 12, color: AppColors.secondaryColor),
                              ),
                              SizedBox(height: 8),
                              Text(
                                widget.rider.regNumber,
                                style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Text(
                                "Gender",
                                style: TextStyle(fontSize: 12, color: AppColors.secondaryColor),
                              ),
                              SizedBox(height: 8),
                              Text(
                                widget.rider.gender,
                                style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: 27),
                  ],
                ),
                Text(
                  "Customer Reviews",
                  style: TextStyle(fontSize: 12, color: Theme.of(context).hintColor),
                ),
                SizedBox(height: 8),
                Container(
                  height: 190,
                  child: ListView.builder(
                    controller: _scrollController,
                    itemCount: reviewList.length,
                    shrinkWrap: true,
                    itemBuilder: (builder, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 2.5, horizontal: 2),
                        child: ReviewCard(
                          fullName: reviewList[index].fullName,
                          review: reviewList[index].review,
                          rating: reviewList[index].rating,
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          reviewList.add(Review("ANOTHER REVIEW", "NICE REVIEW", 5.0));
                          _scrollController.animateTo(10000, duration: Duration(seconds: 1), curve: Curves.bounceIn);
                        });
                      },
                      child: Text(
                        "View More",
                        style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.primaryColor : Colors.white),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 16),
                Row(
                  children: [
                    Expanded(
                      child: AppButton(
                        text:
                            Text("Send Message", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Theme.of(context).scaffoldBackgroundColor)),
                        background: AppColors.primaryColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8), side: BorderSide(color: AppColors.primaryColor)),
                        padding: EdgeInsets.symmetric(vertical: 16),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (_) => ChatScreen(rider: widget.rider)));
                        },
                        radius: 8,
                      ),
                    ),
                    SizedBox(width: 24),
                    Expanded(
                      child: AppButton(
                        text: Text("Call Rider",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Theme.of(context).brightness == Brightness.light ? AppColors.primaryColor : Colors.white)),
                        background: Theme.of(context).scaffoldBackgroundColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(color: Theme.of(context).brightness == Brightness.light ? AppColors.primaryColor : Colors.white)),
                        padding: EdgeInsets.symmetric(vertical: 16),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (_) => CallRiderScreen(rider: widget.rider)));
                        },
                        radius: 8,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ]),
      ),
    ));
  }
}
