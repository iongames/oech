import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/view/screens/home_screen.dart';
import 'package:oech/view/screens/main_screen.dart';
import 'package:oech/view/screens/register_screen.dart';
import 'package:provider/provider.dart';

import '../../viewmodel/login_viewmodel.dart';
import '../colors.dart';
import '../widgets/app_alert_dialog.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';
import 'forgot_screen.dart';
import 'home_screen.dart';

/*
Описание: Класс экрана входа в акканут
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

/*
Описание: State Класса экрана входа в акканут
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _LoginScreenState extends State<LoginScreen> {
  late LoginViewModel viewModel;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _checked = false;
  bool _hide = true;

  /*
  Описание: Метод инициализации класса
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 110),
                  Text(
                    "Welcome Back",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Fill in your email and password to continue",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 48),
                  Text(
                    "Email Address",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    placeholder: "***********@mail.com",
                    controller: _emailController,
                  ),
                  SizedBox(height: 24),
                  Text(
                    "Password",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    placeholder: "**********",
                    controller: _passwordController,
                    obscureText: _hide,
                    icon: InkWell(
                        onTap: () {
                          setState(() {
                            _hide = !_hide;
                          });
                        },
                        child: SvgPicture.asset("assets/icons/ic_eye.svg")),
                  ),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: 14,
                            height: 14,
                            child: Checkbox(
                              value: _checked,
                              onChanged: (value) {
                                setState(() {
                                  _checked = !_checked;
                                });
                              },
                              activeColor: AppColors.primaryColor,
                              side: BorderSide(width: 2, color: AppColors.grayColor),
                            ),
                          ),
                          SizedBox(width: 11),
                          Text(
                            "Remember password",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                              color: AppColors.grayColor,
                            ),
                          ),
                        ],
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (_) => ForgotScreen()));
                        },
                        child: Text(
                          "Forgot Password?",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: AppColors.primaryColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 64),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: AppButton(
                        text: Text(
                          "Log in",
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),
                        ),
                        onTap: () async {
                          if (RegExp(r"^[a-z0-9]*@[a-z0-9]*\.[a-z0-9]*$").hasMatch(_emailController.text)) {
                            showDialog(
                              context: context,
                              builder: (builder) {
                                return const Center(child: CircularProgressIndicator());
                              },
                            );

                            bool result = await viewModel.signIn(_emailController.text, _passwordController.text);

                            if (result) {
                              Navigator.push(context, MaterialPageRoute(builder: (_) => const MainScreen()));
                            }
                          } else {
                            showDialog(
                              context: context,
                              builder: (builder) {
                                return AppAlertDialog(title: "ERROR", content: "Неправильный формат E-Mail!");
                              },
                            );
                          }
                        },
                      ))
                    ],
                  ),
                  SizedBox(height: 8),
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => const RegisterScreen()));
                    },
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Already have an account? ",
                            style: TextStyle(fontSize: 14, color: AppColors.grayColor),
                          ),
                          TextSpan(
                            text: "Sign Up",
                            style: TextStyle(fontSize: 14, color: AppColors.primaryColor),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 30),
                  Column(
                    children: [
                      Text(
                        "or log in using",
                        style: TextStyle(
                          fontSize: 14,
                          color: AppColors.grayColor,
                        ),
                      ),
                      SizedBox(height: 8),
                      SvgPicture.asset("assets/icons/ic_google.svg"),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
