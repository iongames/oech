import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/main.dart';
import 'package:oech/model/service_item.dart';
import 'package:oech/view/colors.dart';
import 'package:oech/view/screens/send_package_screen.dart';
import 'package:oech/view/widgets/app_text_field.dart';
import 'package:oech/view/widgets/service_card.dart';

import '../widgets/greeting_card.dart';
import 'book_rider.dart';

/*
Описание: Класс домашнего экрана 
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

/*
Описание: State Класса домашнего экрана 
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _HomeScreenState extends State<HomeScreen> {
  String name = "";

  List<ServiceItem> serviceList = [
    ServiceItem(
      "Customer Care",
      "Our customer care service line is available from 8 -9pm week days and 9 - 5 weekends - tap to call us today",
      "assets/icons/ic_customer.svg",
    ),
    ServiceItem(
      "Send a package",
      "Request for a driver to pick up or deliver your package for you",
      "assets/icons/ic_package.svg",
    ),
    ServiceItem(
      "Fund your wallet",
      "To fund your wallet is as easy as ABC, make use of our fast technology and top-up your wallet today",
      "assets/icons/ic_fund.svg",
    ),
    ServiceItem(
      "Book a rider",
      "Search for available rider within your area",
      "assets/icons/ic_book.svg",
    ),
    ServiceItem(
      "Enroll as a rider",
      "A chance for you to earn as you become one of our delivery agents, enroll and get the necessary trainings from our crew to get started.",
      "assets/icons/ic_enroll.svg",
    ),
    ServiceItem(
      "Refer and earn",
      "Refer a friend to our platform and stand the chance of winning lots of goodies plus free delivery",
      "assets/icons/ic_program.svg",
    ),
  ];

  @override
  void initState() {
    super.initState();

    if (supabase.auth.currentUser != null) {
      name = supabase.auth.currentUser!.userMetadata!["Full Name"];
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 24),
            HomeTextField(
              placeholder: "Search services",
              icon: SvgPicture.asset("assets/icons/ic_search.svg"),
            ),
            SizedBox(height: 24),
            GreetingCard(
              name: name,
            ),
            SizedBox(height: 39),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Special for you",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.secondaryColor),
                ),
                SvgPicture.asset("assets/icons/ic_next.svg")
              ],
            ),
            SizedBox(height: 7),
            SizedBox(
              height: 64,
              child: ListView.builder(
                itemCount: 5,
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemBuilder: (itemBuilder, index) {
                  return Padding(
                    padding: const EdgeInsets.only(right: 12),
                    child: Image.asset("assets/images/frame_5$index.png"),
                  );
                },
              ),
            ),
            SizedBox(height: 29),
            Text(
              "What would you like to do",
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
            ),
            SizedBox(height: 12),
            Expanded(
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, crossAxisSpacing: 24, mainAxisSpacing: 24),
                  itemCount: 6,
                  itemBuilder: (itemBuilder, index) {
                    return ServiceCard(
                      title: serviceList[index].title,
                      content: serviceList[index].content,
                      asset: serviceList[index].asset,
                      onTap: () {
                        switch (index) {
                          case 1:
                            Navigator.push(context, MaterialPageRoute(builder: (_) => const SendPackageScreen()));
                            break;
                          case 3:
                            Navigator.push(context, MaterialPageRoute(builder: (_) => const BookRiderScreen()));
                            break;
                          default:
                        }
                      },
                    );
                  }),
            ),
            SizedBox(height: 80)
          ],
        ),
      ),
    ));
  }
}
