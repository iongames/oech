import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:oech/model/delivery.dart';
import 'package:oech/view/screens/success_screen.dart';
import 'package:oech/view/widgets/app_button.dart';

import '../colors.dart';

/*
Описание: Класс экрана проверки информации доставки
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class PackageInfoScreen extends StatefulWidget {
  final Delivery delivery;

  const PackageInfoScreen({super.key, required this.delivery});

  @override
  State<PackageInfoScreen> createState() => _PackageInfoScreenState();
}

/*
Описание: State Класса экрана проверки информации доставки
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class _PackageInfoScreenState extends State<PackageInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
          automaticallyImplyLeading: false,
          toolbarHeight: 60,
          title: const Text(
            "Send a package",
            style: TextStyle(fontSize: 16, color: AppColors.grayColor),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 24),
                Text(
                  "Package Information",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
                ),
                SizedBox(height: 8),
                Text(
                  "Origin details",
                  style:
                      TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.lTextColor : AppColors.primaryVariantColor),
                ),
                SizedBox(height: 4),
                Text(
                  "${widget.delivery.startLocation.address} ${widget.delivery.startLocation.state}",
                  style: TextStyle(
                    fontSize: 12,
                    color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  widget.delivery.startLocation.phone,
                  style: TextStyle(
                    fontSize: 12,
                    color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  "Destination details",
                  style:
                      TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.lTextColor : AppColors.primaryVariantColor),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(widget.delivery.endLocations.length, (index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${widget.delivery.endLocations[index].address} ${widget.delivery.endLocations[index].state}",
                          style: TextStyle(
                            fontSize: 12,
                            color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                          ),
                        ),
                        SizedBox(height: 4),
                        Text(
                          widget.delivery.endLocations[index].phone,
                          style: TextStyle(
                            fontSize: 12,
                            color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                          ),
                        ),
                        SizedBox(height: 8),
                      ],
                    );
                  }),
                ),
                Text(
                  "Other details",
                  style:
                      TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.lTextColor : AppColors.primaryVariantColor),
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Package Items",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      widget.delivery.packageItems,
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Weight of items",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      widget.delivery.packageWeight,
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tracking Number",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      widget.delivery.trackingNumber,
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 37),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 1,
                      color: AppColors.grayColor,
                    ))
                  ],
                ),
                SizedBox(height: 8),
                Text(
                  "Charges",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Delivery Charges",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      "N${NumberFormat("###,###,###.00").format(2500.00)}",
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Instant delivery",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      "N${NumberFormat("###,###,###.00").format(300.00)}",
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tax and Service Charges",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      "N${NumberFormat("###,###,###.00").format(200.00)}",
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 9),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 1,
                      color: AppColors.grayColor,
                    ))
                  ],
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Package total",
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                      ),
                    ),
                    Text(
                      "N${NumberFormat("###,###,###.00").format(3000.00)}",
                      style: const TextStyle(
                        fontSize: 12,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 46),
                Row(
                  children: [
                    Expanded(
                      child: AppButton(
                        text: const Text("Edit package", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: AppColors.primaryColor)),
                        background: Theme.of(context).scaffoldBackgroundColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8), side: BorderSide(color: AppColors.primaryColor)),
                        padding: EdgeInsets.symmetric(vertical: 16),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    SizedBox(width: 24),
                    Expanded(
                      child: AppButton(
                        text:
                            Text("Make payment", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: Theme.of(context).scaffoldBackgroundColor)),
                        background: AppColors.primaryColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8), side: BorderSide(color: AppColors.primaryColor)),
                        padding: EdgeInsets.symmetric(vertical: 16),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (_) => SuccessScreen(delivery: widget.delivery)));
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
