import 'package:flutter/material.dart';
import 'package:oech/view/screens/login_screen.dart';
import 'package:provider/provider.dart';

import '../../viewmodel/login_viewmodel.dart';
import '../colors.dart';
import '../widgets/app_alert_dialog.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';
import 'otp_screen.dart';

/*
Описание: Класс экрана ввода Email для восстановления пароля
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class ForgotScreen extends StatefulWidget {
  const ForgotScreen({super.key});

  @override
  State<ForgotScreen> createState() => _ForgotScreenState();
}

/*
Описание: State класса экрана ввода Email для восстановления пароля
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _ForgotScreenState extends State<ForgotScreen> {
  late LoginViewModel viewModel;

  final TextEditingController _emailController = TextEditingController();

  /*
  Описание: Метод инициализации класса
  Дата создания: 01.07.2023
  Автор: Хасанов Георгий
  */
  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 110),
                  Text(
                    "Forgot Password",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Enter your email address",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 48),
                  Text(
                    "Email Address",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    placeholder: "***********@mail.com",
                    controller: _emailController,
                  ),
                  SizedBox(height: 64),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: AppButton(
                        text: Text(
                          "Send OTP",
                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),
                        ),
                        onTap: () async {
                          if (RegExp(r"^[a-z0-9]*@[a-z0-9]*\.[a-z0-9]*$").hasMatch(_emailController.text)) {
                            showDialog(
                              context: context,
                              builder: (builder) {
                                return const Center(child: CircularProgressIndicator());
                              },
                            );

                            bool result = await viewModel.forgotPassword(_emailController.text);

                            if (result) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => OTPScreen(
                                    email: _emailController.text,
                                  ),
                                ),
                              );
                            }
                          } else {
                            showDialog(
                              context: context,
                              builder: (builder) {
                                return AppAlertDialog(title: "ERROR", content: "Неправильный формат E-Mail!");
                              },
                            );
                          }
                        },
                      ))
                    ],
                  ),
                  SizedBox(height: 8),
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
                    },
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Remember password? Back to ",
                            style: TextStyle(fontSize: 14, color: AppColors.grayColor),
                          ),
                          TextSpan(
                            text: "Sign in",
                            style: TextStyle(fontSize: 14, color: AppColors.primaryColor),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
