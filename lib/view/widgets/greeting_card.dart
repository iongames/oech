import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

/*
Описание: Приветсвенная карточка главного экрана
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class GreetingCard extends StatelessWidget {
  final String name;

  const GreetingCard({super.key, required this.name});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      clipBehavior: Clip.antiAlias,
      child: Stack(children: [
        Image.asset(Theme.of(context).brightness == Brightness.light ? "assets/images/frame_light.png" : "assets/images/frame_dark.png"),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Hello $name",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Colors.white),
                  ),
                  Text(
                    "We trust you are having a great time",
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  ),
                ],
              ),
              SvgPicture.asset("assets/icons/ic_notification.svg")
            ],
          ),
        )
      ]),
    );
  }
}
