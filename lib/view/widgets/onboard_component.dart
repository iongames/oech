import 'package:flutter/material.dart';
import 'package:oech/model/onboard_item.dart';
import 'package:oech/view/widgets/app_button.dart';

import '../colors.dart';
import '../screens/login_screen.dart';
import '../screens/register_screen.dart';

/*
Описание: Элемент приветственного экрана
Дата создания: 0.07.2023
Автор: Хасанов Георгий
*/
class OnboardComponent extends StatelessWidget {
  final OnboardItem onboardItem;
  final int itemIndex;
  final Function() onSkip;
  final Function() onLogin;
  final Function() onNext;

  const OnboardComponent({super.key, required this.onboardItem, required this.itemIndex, required this.onSkip, required this.onNext, required this.onLogin});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        //height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 22),
                child: Column(
                  children: [
                    SizedBox(height: 66),
                    Container(
                      height: 346,
                      child: Image.asset(onboardItem.asset),
                      alignment: Alignment.center,
                    ),
                    Container(
                      height: 60,
                      child: Text(
                        onboardItem.title,
                        style: const TextStyle(fontSize: 24, fontWeight: FontWeight.w700, color: AppColors.primaryColor),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(
                      onboardItem.content,
                      style: TextStyle(fontSize: 16, color: Theme.of(context).hintColor),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 40),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(3, (index) {
                        return _RoundDot(
                          itemIndex: itemIndex,
                          index: index,
                        );
                      }),
                    ),
                  ],
                ),
              ),
              itemIndex != 2
                  ? Padding(
                      padding: const EdgeInsets.only(bottom: 88, top: 82),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AppButton(
                            key: itemIndex == 0 ? const Key("skip1") : const Key("skip2"),
                            text: _ButtonText(text: "Skip", color: AppColors.primaryColor),
                            background: Theme.of(context).scaffoldBackgroundColor,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4), side: BorderSide(color: AppColors.primaryColor)),
                            onTap: onSkip,
                            padding: EdgeInsets.symmetric(vertical: 9.38, horizontal: 18),
                          ),
                          AppButton(
                            text: _ButtonText(text: "Next", color: Colors.white),
                            background: AppColors.primaryColor,
                            onTap: onNext,
                            padding: EdgeInsets.symmetric(vertical: 9.38, horizontal: 18),
                          ),
                        ],
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.only(bottom: 68, top: 62),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: AppButton(
                                  text: Text(
                                    "Sign Up",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                  background: AppColors.primaryColor,
                                  onTap: onSkip,
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 8),
                          InkWell(
                            onTap: onLogin,
                            child: RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Already have an account? ",
                                    style: TextStyle(fontSize: 14, color: AppColors.grayColor),
                                  ),
                                  TextSpan(
                                    text: "Sign in",
                                    style: TextStyle(fontSize: 14, color: AppColors.primaryColor),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
            ],
          ),
        ),
      ),
    );
  }
}

class _ButtonText extends StatelessWidget {
  final String text;
  final Color color;

  const _ButtonText({
    super.key,
    required this.color,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(fontSize: 9.38, fontWeight: FontWeight.w700, color: color),
    );
  }
}

/*
Описание: Элемент индикатора страницы
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _RoundDot extends StatelessWidget {
  const _RoundDot({
    super.key,
    required this.itemIndex,
    required this.index,
  });

  final int itemIndex;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.all(4),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
      clipBehavior: Clip.antiAlias,
      child: Container(
        width: 8,
        height: 8,
        color: itemIndex == index ? AppColors.primaryColor : AppColors.grayColor,
      ),
    );
  }
}
