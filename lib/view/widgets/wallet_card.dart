import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/view/colors.dart';

/*
Описание: Элемент карточки кошелька
Дата создания: 03.07.2023
Автор: Хасанов Георгий
*/
class WalletCard extends StatelessWidget {
  const WalletCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      color: Theme.of(context).cardColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 48, vertical: 10),
        child: Column(
          children: [
            Text(
              "Top Up",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Theme.of(context).hintColor),
            ),
            SizedBox(height: 12),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                WalletItem(
                  title: "Bank",
                  asset: "assets/icons/ic_bank.svg",
                  onTap: () {},
                ),
                WalletItem(
                  title: "Transfer",
                  asset: "assets/icons/ic_transfer.svg",
                  onTap: () {},
                ),
                WalletItem(
                  title: "Card",
                  asset: "assets/icons/ic_card.svg",
                  onTap: () {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

/*
Описание: Элемент карточки кошелька
Дата создания: 03.07.2023
Автор: Хасанов Георгий
*/
class WalletItem extends StatelessWidget {
  final String title;
  final String asset;
  final Function() onTap;

  const WalletItem({
    super.key,
    required this.title,
    required this.asset,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Container(
            height: 48,
            width: 48,
            child: Card(
              elevation: 0,
              margin: EdgeInsets.zero,
              color: AppColors.primaryColor,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
              clipBehavior: Clip.antiAlias,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    asset,
                    color: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
                    width: 20,
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 4),
          Text(
            title,
            style: TextStyle(fontSize: 12, color: Theme.of(context).hintColor),
          )
        ],
      ),
    );
  }
}

/*
Описание: Карточка сервиса в профиле
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class TransactionCard extends StatelessWidget {
  final String title;
  final String date;
  final String sum;
  final bool type;

  const TransactionCard({
    super.key,
    required this.title,
    required this.date,
    required this.sum,
    required this.type,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(),
      color: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                ),
                SizedBox(height: 4),
                Text(
                  date,
                  style: TextStyle(fontSize: 12, color: AppColors.grayColor),
                ),
              ],
            ),
            Text(
              sum,
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: type ? AppColors.greenColor : AppColors.errorColor),
            )
          ],
        ),
      ),
    );
  }
}
