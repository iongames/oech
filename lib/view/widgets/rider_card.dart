import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/model/rider.dart';
import 'package:oech/view/colors.dart';

/*
Описание: Карточка доставщика
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class RiderCard extends StatefulWidget {
  final Rider rider;
  final Function() onTap;

  const RiderCard({super.key, required this.rider, required this.onTap});

  @override
  State<RiderCard> createState() => _RiderCardState();
}

class _RiderCardState extends State<RiderCard> {
  Color fillColor = Colors.white;
  Color borderColor = Colors.white;
  Color textColor = Colors.white;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    fillColor = Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor;
    borderColor = Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor;
    textColor = Theme.of(context).hintColor;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      margin: EdgeInsets.zero,
      color: fillColor,
      shape: RoundedRectangleBorder(side: BorderSide(width: 2, color: borderColor)),
      child: InkWell(
        onTap: widget.onTap,
        onTapDown: (details) {
          setState(() {
            fillColor = Theme.of(context).brightness == Brightness.light ? AppColors.lCardColor : Color(0xFF002858);
            borderColor = AppColors.primaryColor;
            textColor = AppColors.successDeliveryColor;
          });
        },
        onTapUp: (details) {
          setState(() {
            fillColor = Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor;
            borderColor = Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor;
            textColor = Theme.of(context).hintColor;
          });
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(width: 2),
                  Card(
                    elevation: 0,
                    margin: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                    color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                    clipBehavior: Clip.antiAlias,
                    child: Padding(
                      padding: const EdgeInsets.all(2),
                      child: const SizedBox(height: 60, width: 60),
                    ),
                  ),
                  SizedBox(width: 12),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.rider.fullName,
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: textColor),
                      ),
                      SizedBox(height: 4),
                      Text(
                        widget.rider.regNumber,
                        style: TextStyle(fontSize: 12, color: textColor),
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                children: List.generate(5, (index) {
                  return Row(
                    children: [
                      SvgPicture.asset(
                        "assets/icons/ic_star.svg",
                        width: 9,
                        height: 9,
                        color: index <= widget.rider.rating
                            ? AppColors.primaryColor
                            : Theme.of(context).brightness == Brightness.light
                                ? AppColors.grayColor
                                : Colors.white,
                      ),
                      index != 4 ? SizedBox(width: 8) : SizedBox()
                    ],
                  );
                }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/*
Описание: Карточка отзыва
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class ReviewCard extends StatelessWidget {
  final String fullName;
  final String review;
  final double rating;

  const ReviewCard({
    super.key,
    required this.fullName,
    required this.review,
    required this.rating,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      margin: EdgeInsets.zero,
      color: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
      shape: RoundedRectangleBorder(),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Card(
                  elevation: 0,
                  margin: EdgeInsets.zero,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                  color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white,
                  clipBehavior: Clip.antiAlias,
                  child: Padding(
                    padding: const EdgeInsets.all(2),
                    child: const SizedBox(height: 60, width: 60),
                  ),
                ),
                SizedBox(width: 12),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      fullName,
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                    ),
                    SizedBox(height: 4),
                    Text(
                      review,
                      style: TextStyle(fontSize: 12, color: Theme.of(context).hintColor),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              children: List.generate(5, (index) {
                return Row(
                  children: [
                    SvgPicture.asset(
                      "assets/icons/ic_star.svg",
                      width: 9,
                      height: 9,
                      color: index <= rating
                          ? Theme.of(context).brightness == Brightness.light
                              ? AppColors.primaryColor
                              : AppColors.secondaryColor
                          : Theme.of(context).brightness == Brightness.light
                              ? AppColors.grayColor
                              : Colors.white,
                    ),
                    SizedBox(width: 8)
                  ],
                );
              }),
            ),
          ],
        ),
      ),
    );
  }
}
