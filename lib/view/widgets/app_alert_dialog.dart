import 'package:flutter/material.dart';

/*
Описание: Диалоговое окно с предупреждением
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class AppAlertDialog extends StatelessWidget {
  final String title;
  final String content;

  const AppAlertDialog({super.key, required this.title, required this.content});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text("OK"),
        )
      ],
    );
  }
}
