import 'package:flutter/material.dart';
import 'package:oech/view/colors.dart';

import '../../model/message.dart';

/*
Описание: Карточка сообщения в чате
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class MessageCard extends StatelessWidget {
  final Message message;

  const MessageCard({super.key, required this.message});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: message.type ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        Container(
          constraints: message.type
              ? BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.6)
              : BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.6, minWidth: MediaQuery.of(context).size.width * 0.6),
          child: Card(
            elevation: 0,
            margin: EdgeInsets.zero,
            color: message.type ? AppColors.primaryColor : Theme.of(context).cardColor,
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                message.message,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                  color: message.type ? Colors.white : Theme.of(context).hintColor,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
