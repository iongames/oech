import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/view/colors.dart';

/*
Описание: Элемент сервиса главного экрана
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class ServiceCard extends StatefulWidget {
  final String title;
  final String content;
  final String asset;
  final Function() onTap;

  const ServiceCard({super.key, required this.title, required this.content, required this.asset, required this.onTap});

  @override
  State<ServiceCard> createState() => _ServiceCardState();
}

/*
Описание: State Элемента сервиса главного экрана
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class _ServiceCardState extends State<ServiceCard> {
  Color titleColor = AppColors.primaryColor;
  Color textColor = Colors.white;
  Color cardColor = AppColors.lCardColor;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    cardColor = Theme.of(context).brightness == Brightness.light ? AppColors.lGrayColor : AppColors.dCardColor;
    textColor = Theme.of(context).hintColor;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 159,
      width: 159,
      child: Card(
        elevation: 5,
        shadowColor: Colors.black.withOpacity(0.5),
        color: cardColor,
        margin: EdgeInsets.zero,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: widget.onTap,
          splashFactory: NoSplash.splashFactory,
          onTapDown: (value) {
            setState(() {
              titleColor = Colors.white;
              textColor = Colors.white;
              cardColor = AppColors.primaryColor;
            });
          },
          onTapUp: (value) {
            setState(() {
              titleColor = AppColors.primaryColor;
              cardColor = Theme.of(context).brightness == Brightness.light ? AppColors.lGrayColor : AppColors.dCardColor;
              textColor = Theme.of(context).hintColor;
            });
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20),
                SvgPicture.asset(
                  widget.asset,
                  width: 36,
                  height: 36,
                  color: titleColor,
                ),
                SizedBox(height: 6),
                Text(
                  widget.title,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: titleColor,
                  ),
                ),
                SizedBox(height: 6),
                Text(
                  widget.content,
                  style: TextStyle(
                    fontSize: 7.45,
                    color: textColor,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
