import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/view/colors.dart';

/*
Описание: Карточка сервиса в профиле
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class ProfileCard extends StatelessWidget {
  final String title;
  final String content;
  final String icon;
  final Function() onTap;

  const ProfileCard({super.key, required this.title, required this.content, required this.icon, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(),
      color: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SvgPicture.asset(
                    "assets/icons/$icon.svg",
                    color: Theme.of(context).hintColor,
                  ),
                  SizedBox(width: 9),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                      ),
                      SizedBox(height: 4),
                      Text(
                        content,
                        style: TextStyle(fontSize: 12, color: AppColors.grayColor),
                      ),
                    ],
                  ),
                ],
              ),
              SvgPicture.asset(
                "assets/icons/ic_next_ios.svg",
                color: Theme.of(context).hintColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/*
Описание: Карточка выхода из аккаунта в профиле
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class LogoutCard extends StatelessWidget {
  final Function() onLogout;

  const LogoutCard({super.key, required this.onLogout});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      margin: EdgeInsets.zero,
      shape: const RoundedRectangleBorder(),
      color: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
      child: InkWell(
        onTap: onLogout,
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SvgPicture.asset(
                    "assets/icons/ic_logout.svg",
                    color: AppColors.errorColor,
                  ),
                  SizedBox(width: 9),
                  Text(
                    "Log out",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  ),
                ],
              ),
              SvgPicture.asset(
                "assets/icons/ic_next_ios.svg",
                color: Theme.of(context).hintColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
