import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech/view/colors.dart';

/*
Описание: Текстовое поле приложения
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class AppTextField extends StatelessWidget {
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final String placeholder;
  final Color? fillColor;
  final EdgeInsets padding;
  final TextInputType? keyboardType;
  final bool obscureText;
  final Widget? icon;

  const AppTextField({
    super.key,
    this.fillColor,
    required this.placeholder,
    this.padding = const EdgeInsets.symmetric(horizontal: 10, vertical: 14),
    this.onChanged,
    this.controller,
    this.keyboardType,
    this.obscureText = false,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onChanged: onChanged,
      keyboardType: keyboardType,
      obscureText: obscureText,
      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
      decoration: InputDecoration(
        filled: true,
        fillColor: fillColor ?? Theme.of(context).scaffoldBackgroundColor,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
          borderSide: BorderSide(color: AppColors.lCardColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
          borderSide: BorderSide(color: AppColors.lCardColor),
        ),
        hintText: placeholder,
        hintStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.lCardColor),
        contentPadding: padding,
        suffixIcon: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon ?? SizedBox(),
          ],
        ),
      ),
    );
  }
}

/*
Описание: Текстовое поле экрана OTP
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class CodeTextField extends StatelessWidget {
  final TextEditingController controller;

  const CodeTextField({
    super.key,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: SizedBox(
        width: 32,
        height: 32,
        child: TextFormField(
          controller: controller,
          textAlign: TextAlign.center,
          inputFormatters: [LengthLimitingTextInputFormatter(1)],
          decoration: InputDecoration(
            filled: true,
            fillColor: Theme.of(context).scaffoldBackgroundColor,
            counterText: "",
            contentPadding: EdgeInsets.symmetric(horizontal: 11, vertical: 8),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.zero,
              borderSide: BorderSide(color: AppColors.secondaryColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.zero,
              borderSide: BorderSide(color: AppColors.secondaryColor),
            ),
          ),
        ),
      ),
    );
  }
}

/*
Описание: Текстовое поле главного экрана
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class HomeTextField extends StatelessWidget {
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final String placeholder;
  final Widget? icon;

  const HomeTextField({
    super.key,
    required this.placeholder,
    this.onChanged,
    this.controller,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onChanged: onChanged,
      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
      decoration: InputDecoration(
        filled: true,
        fillColor: Theme.of(context).cardColor,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
          borderSide: BorderSide(color: AppColors.lCardColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
          borderSide: BorderSide(color: AppColors.lCardColor),
        ),
        hintText: placeholder,
        hintStyle: TextStyle(fontSize: 12, color: AppColors.grayColor),
        contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 9),
        suffixIcon: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon ?? SizedBox(),
          ],
        ),
      ),
    );
  }
}

/*
Описание: Текстовое поле доставки
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class DeliveryTextField extends StatelessWidget {
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final String placeholder;

  const DeliveryTextField({
    super.key,
    required this.placeholder,
    this.onChanged,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      shape: RoundedRectangleBorder(),
      margin: EdgeInsets.zero,
      child: TextField(
        controller: controller,
        onChanged: onChanged,
        style: TextStyle(fontSize: 12, color: Theme.of(context).hintColor),
        decoration: InputDecoration(
          filled: true,
          fillColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(0),
            borderSide: BorderSide(color: Colors.transparent),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(0),
            borderSide: BorderSide(color: Colors.transparent),
          ),
          hintText: placeholder,
          hintStyle: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.lCardColor : Colors.white),
          contentPadding: EdgeInsets.all(8),
        ),
      ),
    );
  }
}

/*
Описание: Текстовое поле оценки
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class RateTextField extends StatelessWidget {
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final String placeholder;

  const RateTextField({
    super.key,
    required this.placeholder,
    this.onChanged,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      shape: RoundedRectangleBorder(),
      color: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
      margin: EdgeInsets.zero,
      child: TextField(
        controller: controller,
        onChanged: onChanged,
        maxLength: 10,
        style: TextStyle(fontSize: 12, color: Theme.of(context).hintColor),
        decoration: InputDecoration(
            filled: true,
            fillColor: Theme.of(context).brightness == Brightness.light ? Colors.white : AppColors.dCardColor,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(0),
              borderSide: BorderSide(color: Colors.transparent),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(0),
              borderSide: BorderSide(color: Colors.transparent),
            ),
            counterText: "",
            hintText: placeholder,
            hintStyle: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.lCardColor : Colors.white),
            contentPadding: EdgeInsets.symmetric(vertical: 17),
            icon: Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset("assets/icons/ic_rate.svg"),
                ],
              ),
            )),
      ),
    );
  }
}

/*
Описание: Адаптивное текстовое поле приложения
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class S5TextField extends StatefulWidget {
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final String placeholder;
  final EdgeInsets padding;
  final TextInputType? keyboardType;
  final Widget? icon;

  S5TextField({
    super.key,
    required this.placeholder,
    this.padding = const EdgeInsets.symmetric(horizontal: 10, vertical: 14),
    this.onChanged,
    this.controller,
    this.keyboardType,
    this.icon,
  });

  @override
  State<S5TextField> createState() => _S5TextFieldState();
}

/*
Описание: State Адаптивного текстового поля приложения
Дата создания: 04.07.2023
Автор: Хасанов Георгий
*/
class _S5TextFieldState extends State<S5TextField> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.controller,
      onChanged: widget.onChanged,
      keyboardType: widget.keyboardType,
      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
      decoration: InputDecoration(
        filled: true,
        fillColor: widget.controller!.text.isEmpty
            ? Theme.of(context).cardColor
            : Theme.of(context).brightness == Brightness.light
                ? Theme.of(context).scaffoldBackgroundColor
                : Theme.of(context).cardColor,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
          borderSide: BorderSide(color: AppColors.lCardColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
          borderSide: BorderSide(color: AppColors.lCardColor),
        ),
        hintText: widget.placeholder,
        hintStyle: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.light ? AppColors.grayColor : Colors.white),
        contentPadding: widget.padding,
        suffixIcon: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            widget.icon ?? SizedBox(),
          ],
        ),
      ),
    );
  }
}
