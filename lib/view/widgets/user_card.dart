import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:oech/main.dart';
import 'package:oech/view/colors.dart';

/*
Описание: Класс элемента карточки профиля
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class UserCard extends StatefulWidget {
  const UserCard({super.key});

  @override
  State<UserCard> createState() => _UserCardState();
}

/*
Описание: State Класса элемента карточки профиля
Дата создания: 02.07.2023
Автор: Хасанов Георгий
*/
class _UserCardState extends State<UserCard> {
  String userName = "";
  double userBalance = 10712.00;

  bool _hide = false;

  @override
  void initState() {
    super.initState();

    if (supabase.auth.currentUser != null) {
      userName = supabase.auth.currentUser!.userMetadata!["Full Name"];
      //userBalance = supabase.auth.currentUser!.userMetadata!["balance"];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            SizedBox(
              width: 60,
              height: 60,
              child: Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(1000),
                  side: BorderSide(color: AppColors.grayColor),
                ),
                clipBehavior: Clip.antiAlias,
                child: Image.asset("assets/images/default.png"),
              ),
            ),
            SizedBox(width: 12),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  userName,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).hintColor,
                  ),
                ),
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "Current balance:",
                        style: TextStyle(fontSize: 12, color: Theme.of(context).hintColor),
                      ),
                      TextSpan(
                        text: !_hide ? " N${NumberFormat("###,###,###,###.00").format(userBalance).replaceAll(".", ":")}" : " ******",
                        style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
        InkWell(
          onTap: () {
            setState(() {
              _hide = !_hide;
            });
          },
          child: SvgPicture.asset(
            "assets/icons/ic_eye.svg",
            color: Theme.of(context).hintColor,
          ),
        )
      ],
    );
  }
}
