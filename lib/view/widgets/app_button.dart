import 'package:flutter/material.dart';
import 'package:oech/view/colors.dart';

/*
Описание: Элемент кнопки приложения
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
class AppButton extends StatelessWidget {
  final Widget text;
  final Color background;
  final ShapeBorder? shape;
  final EdgeInsets padding;
  final Function() onTap;
  final double? radius;

  const AppButton({
    super.key,
    required this.text,
    this.background = AppColors.primaryColor,
    this.shape,
    this.padding = const EdgeInsets.symmetric(vertical: 15),
    required this.onTap,
    this.radius = 4,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: background,
      margin: EdgeInsets.zero,
      shape: shape ?? RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius!)),
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: onTap,
        child: Center(
          child: Padding(
            padding: padding,
            child: text,
          ),
        ),
      ),
    );
  }
}
