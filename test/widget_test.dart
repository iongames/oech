// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';

import 'package:oech/main.dart';
import 'package:oech/view/screens/onboard_screen.dart';
import 'package:oech/viewmodel/login_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

/*
Описание: Метод UI-тестов приложения
Дата создания: 01.07.2023
Автор: Хасанов Георгий
*/
void main() {
  testWidgets('Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь).', (WidgetTester tester) async {
    await tester.binding.setSurfaceSize(Size(1440, 3140));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => LoginViewModel())],
      child: GetMaterialApp(home: OnboardScreen()),
    ));

    expect(find.text("Quick Delivery At Your Doorstep"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
    await tester.scrollUntilVisible(find.text("Quick Delivery At Your Doorstep"), 1);
    await tester.pump();

    expect(find.text("Flexible Payment"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
    await tester.scrollUntilVisible(find.text("Flexible Payment"), 1);
    await tester.pump();

    expect(find.text("Real-time Tracking"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
  });

  testWidgets('Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).', (WidgetTester tester) async {
    await tester.binding.setSurfaceSize(Size(1440, 3140));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => LoginViewModel())],
      child: GetMaterialApp(home: OnboardScreen()),
    ));

    expect(find.text("Quick Delivery At Your Doorstep"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
    await tester.scrollUntilVisible(find.text("Quick Delivery At Your Doorstep"), 1);
    await tester.pump();

    expect(find.text("Flexible Payment"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
    await tester.scrollUntilVisible(find.text("Flexible Payment"), 1);
    await tester.pump();

    expect(find.text("Real-time Tracking"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
    await tester.scrollUntilVisible(find.text("Flexible Payment"), 1);
    await tester.pump();

    expect(find.text("Flexible Payment"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
    await tester.scrollUntilVisible(find.text("Flexible Payment"), 1);
    await tester.pump();

    expect(find.text("Real-time Tracking"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
  });

  testWidgets('В случае, когда в очереди несколько картинок, устанавливается правильная надпись на кнопке.', (WidgetTester tester) async {
    await tester.binding.setSurfaceSize(Size(1440, 3140));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => LoginViewModel())],
      child: GetMaterialApp(home: OnboardScreen()),
    ));

    expect(find.byKey(const Key("skip1")), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
    await tester.scrollUntilVisible(find.text("Quick Delivery At Your Doorstep"), 1);
    await tester.pump();

    expect(find.byKey(const Key("skip2")), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
    await tester.scrollUntilVisible(find.text("Flexible Payment"), 1);
    await tester.pump();

    expect(find.text("Sign Up"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
  });

  testWidgets('Случай, когда в очереди осталось только одно изображение, надпись на кнопке должна измениться на "Завершить".', (WidgetTester tester) async {
    await tester.binding.setSurfaceSize(Size(1440, 3140));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => LoginViewModel())],
      child: GetMaterialApp(home: OnboardScreen()),
    ));

    await tester.scrollUntilVisible(find.text("Quick Delivery At Your Doorstep"), 1);
    await tester.pump();

    await tester.scrollUntilVisible(find.text("Flexible Payment"), 1);
    await tester.pump();

    expect(find.text("Sign Up"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
  });

  testWidgets(
      'Если очередь пустая и пользователь нажал на кнопку “Вход”, происходит открытие экрана Вход приложения. Если очередь не пустая – переход отсутствует.',
      (WidgetTester tester) async {
    await tester.binding.setSurfaceSize(Size(1440, 3140));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => LoginViewModel())],
      child: GetMaterialApp(home: OnboardScreen()),
    ));

    await tester.scrollUntilVisible(find.text("Quick Delivery At Your Doorstep"), 1);
    await tester.pump();

    await tester.scrollUntilVisible(find.text("Flexible Payment"), 1);
    await tester.pump();

    expect(find.text("Sign Up"), findsOneWidget); // AppLocalizations.of(Get.context).onboard_1
    await tester.tap(find.text("Sign Up"));
    await tester.pumpAndSettle();

    expect(find.text("Sign Up"), findsNothing); // AppLocalizations.of(Get.context).onboard_1
  });

  testWidgets('Наличие вызова метода сохранения флага об успешном прохождении приветствия пользователем.', (WidgetTester tester) async {
    await tester.binding.setSurfaceSize(Size(1440, 3140));

    await tester.pumpWidget(MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => LoginViewModel())],
      child: GetMaterialApp(home: OnboardScreen()),
    ));

    SharedPreferences.setMockInitialValues({});
    SharedPreferences shared = await SharedPreferences.getInstance();

    await tester.tap(find.byKey(const Key("skip1")));
    await tester.pumpAndSettle();

    expect(shared.getBool("isFirstEnter"), false);
  });
}
